<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('hotels', 'Hotels');
Route::model('hotelsfacilities', 'Hotelsfacilities');
Route::model('hotelsimages', 'Hotelsimages');
Route::model('roomfacilities', 'Roomfacilities');
Route::model('roomtypes', 'Roomtypes');
Route::model('activities', 'Activities');
Route::model('activitiesplaces', 'Activitiesplaces');
Route::model('booking_activities', 'Bookingactivities');
Route::model('booking_rentacar', 'Bookingrentacar');
Route::model('booking_person', 'Bookingperson');
Route::model('typeofcar', 'Typeofcar');
Route::model('packages', 'Packages');
Route::model('amenities', 'Amenities');

Route::model('role', 'Role');


/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
    Route::controller('comments', 'AdminCommentsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
    Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
    Route::controller('blogs', 'AdminBlogsController');
    
    # Hotels Management
    Route::get('hotels/{hotels}/show', 'AdminHotelsController@getShow');
    Route::get('hotels/{hotels}/edit', 'AdminHotelsController@getEdit');
    Route::post('hotels/{hotels}/edit', 'AdminHotelsController@postEdit');
    Route::get('hotels/{hotels}/delete', 'AdminHotelsController@getDelete');
    Route::post('hotels/{hotels}/delete', 'AdminHotelsController@postDelete');
    Route::controller('hotels', 'AdminHotelsController');

    # Packages Management
    Route::get('packages/{packages}/show', 'AdminPackagesController@getShow');
    Route::get('packages/{packages}/edit', 'AdminPackagesController@getEdit');
    Route::post('packages/{packages}/edit', 'AdminPackagesController@postEdit');
    Route::get('packages/{packages}/delete', 'AdminPackagesController@getDelete');
    Route::post('packages/{packages}/delete', 'AdminPackagesController@postDelete');
    Route::controller('packages', 'AdminPackagesController');

    # Amenities Management
    Route::get('amenities/{amenities}/show', 'AdminAmenitiesController@getShow');
    Route::get('amenities/{amenities}/edit', 'AdminAmenitiesController@getEdit');
    Route::post('amenities/{amenities}/edit', 'AdminAmenitiesController@postEdit');
    Route::get('amenities/{amenities}/delete', 'AdminAmenitiesController@getDelete');
    Route::post('amenities/{amenities}/delete', 'AdminAmenitiesController@postDelete');
    Route::controller('amenities', 'AdminAmenitiesController');

    # Activities Management
    Route::get('activities/{activities}/show', 'AdminActivitiesController@getShow');
    Route::get('activities/{activities}/edit', 'AdminActivitiesController@getEdit');
    Route::post('activities/{activities}/edit', 'AdminActivitiesController@postEdit');
    Route::get('activities/{activities}/delete', 'AdminActivitiesController@getDelete');
    Route::post('activities/{activities}/delete', 'AdminActivitiesController@postDelete');
    Route::controller('activities', 'AdminActivitiesController');

    # Activities Places Management
    Route::get('activitiesplaces/{activitiesplaces}/show', 'AdminActivitiesplacesController@getShow');
    Route::get('activitiesplaces/{activitiesplaces}/edit', 'AdminActivitiesplacesController@getEdit');
    Route::post('activitiesplaces/{activitiesplaces}/edit', 'AdminActivitiesplacesController@postEdit');
    Route::get('activitiesplaces/{activitiesplaces}/delete', 'AdminActivitiesplacesController@getDelete');
    Route::post('activitiesplaces/{activitiesplaces}/delete', 'AdminActivitiesplacesController@postDelete');
    Route::controller('activitiesplaces', 'AdminActivitiesplacesController');

    # Type of Car Management
    Route::get('typeofcar/{typeofcar}/show', 'AdminTypeofcarController@getShow');
    Route::get('typeofcar/{typeofcar}/edit', 'AdminTypeofcarController@getEdit');
    Route::post('typeofcar/{typeofcar}/edit', 'AdminTypeofcarController@postEdit');
    Route::get('typeofcar/{typeofcar}/delete', 'AdminTypeofcarController@getDelete');
    Route::post('typeofcar/{typeofcar}/delete', 'AdminTypeofcarController@postDelete');
    Route::controller('typeofcar', 'AdminTypeofcarController');

    #Booking Management
        Route::group(array('prefix' => 'booking'), function()
        {
            #Rent a Car Booking Management
            Route::get('rentacar/{booking_rentacar}/show', 'AdminBookingrentacarController@getShow');
            Route::get('rentacar/{booking_rentacar}/edit', 'AdminBookingrentacarController@getEdit');
            Route::post('rentacar/{booking_rentacar}/edit', 'AdminBookingrentacarController@postEdit');
            Route::get('rentacar/{booking_rentacar}/delete', 'AdminBookingrentacarController@getDelete');
            Route::post('rentacar/{booking_rentacar}/delete', 'AdminBookingrentacarController@postDelete');
            Route::controller('rentacar', 'AdminBookingrentacarController');
            
            #Activities Booking Management
            Route::get('activities/{id}/show', 'AdminBookingactivitiesController@getShow');
            Route::get('activities/{id}/edit', 'AdminBookingactivitiesController@getEdit');
            Route::post('activities/{id}/edit', 'AdminBookingactivitiesController@postEdit');
            Route::get('activities/{id}/delete', 'AdminBookingactivitiesController@getDelete');
            Route::post('activities/{id}/delete', 'AdminBookingactivitiesController@postDelete');
            Route::get('activities/{id}/status', 'AdminBookingactivitiesController@getStatus');
            Route::post('activities/{id}/status', 'AdminBookingactivitiesController@postStatus');
            Route::controller('activities', 'AdminBookingactivitiesController');
            
        });
    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */


Route::get('/abcd', function()
{
    $users = Activities::all();

    return Datatable::of($users)->make();
});

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('contact-us', function()
{
    // Return about us page
    return View::make('site/contact-us');
});

Route::get('/hotels','HomeController@getHotels');
Route::get('/activities','HomeController@getActivities');
Route::get('/planatrip','HomeController@getPlanatrip');
Route::get('/rentacar','HomeController@getRentacar');

#Packages Management
Route::get('/packages/{packageslug}','PackagesController@getView');

#Activities Management
Route::get('/activities/data','ActivitiesController@getData');
Route::get('/activities/{activity}','ActivitiesController@getView');
Route::get('/activities/{activity}/{activitydetail}','ActivitiesController@getViewdetail');


#Booking Management
Route::group(array('prefix' => 'book', 'before' => 'auth'), function()
{
    #Booking Activities
    Route::get('activities/{booking}/{package}','BookingController@getActivities');
    Route::post('activities/{booking}/{package}','BookingController@postActivities');
    
    Route::post('rentacar','BookingController@postRentacar');
    #Personal Information
    #Route::get('{from}/{rentacar}','BookingController@getPersonalinfo');
    #Route::post('{from}/{id}','BookingController@postPersonalinfo');

});



# Posts - Second to last set, match slug
Route::get('{postSlug}', 'BlogController@getView');
Route::post('{postSlug}', 'BlogController@postView');

# Index Page - Last route, no matches
Route::get('/', array('before' => 'detectLang','uses' => 'HomeController@getIndex'));
