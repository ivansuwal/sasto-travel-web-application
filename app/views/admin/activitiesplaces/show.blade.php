@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')

	<table  class="table-bordered">
		<tr>
			<td>Name
			</td>
			<td>{{$activitiesplaces->name}}
			</td>
		</tr>
		<tr>
			<td>Description
			</td>
			<td>{{$activitiesplaces->description}}
			</td>
		</tr>
		<tr>
			<td>YouTube Link
			</td>
			<td>{{$activitiesplaces->youtube_link}}
			</td>
		</tr>


		<tr>
			<td>Images
			</td>
			<td>
				<table>
					@foreach($activitiesplacesimages as $i)
					<tr>
						<td><img src={{ URL::to($i->image_path) }} style="width:300px">
						</td>
					</tr>
					@endforeach
				</table>
			</td>
		</tr>

		<tr>
			<td>Packages
			</td>
			<td>
				<table style="border:1px solid #ccc">
					<tr>
						<td>Name
						</td>
						<td>Description
						</td>
						<td>Price
						</td>
					</tr>
					@foreach($activitiesplacespackages as $pack)
					<tr>
						<td>{{$pack->name}}:
						</td>
						<td>{{$pack->description}}
						</td>
						<td>{{$pack->price}}
						</td>
					</tr>
					@endforeach
				</table>
			</td>
		</tr>

	</table>	
@stop