@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($activitiesplaces)){{ URL::to('admin/activitiesplaces/' . $activitiesplaces->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Place Name</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($activitiesplaces) ? $activitiesplaces->name : null) }}}" />
						{{ $errors->first('name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="description">Description</label>
						<input class="form-control" type="textbox" name="description" id="description" value="{{{ Input::old('description', isset($activitiesplaces) ? $activitiesplaces->description : null) }}}" />
						{{ $errors->first('description', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="activities_id">Activity</label>
						<Select name="activities_id" id="activities_id">
							@foreach($activities as $activity)
							<option value={{ $activity->id }} {{{ isset($activitiesplaces) ? (($activitiesplaces->activities_id==$activity->id)? "Selected" : null) : (Input::old('activities_id')==$activity->id)? "Selected" :null }}}>
								{{ $activity->name }}
							</option>
							@endforeach
						 </select>
						{{ $errors->first('activities_id', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('price') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="price">price</label>
						<input class="form-control" type="textbox" name="price" id="price" value="{{{ Input::old('price', isset($activitiesplaces) ? $activitiesplaces->price : null) }}}" />
						{{ $errors->first('price', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2" id="image_1">
                	    <label class="control-label" for="image_1">Image</label>
						<input class="form-control" type="file" name="image[]"/>
				    	<button class="btn btn-success" id="add_image_btn">+</button>
                    </div>
				    
			    </div>
				<div class="form-group">
					<div class="col-md-6" id="packages_1">
                	    <label class="control-label" for="package_1">package</label>
						<input class="form-control" type="text" name="package_name[]" placeholder="Package Title"/>
						<input class="form-control" type="text" name="package_price[]" placeholder="Price"/>
						<input class="form-control" type="text" name="package_description[]" placeholder="Description"/>
						<button class="btn btn-success" id="add_packages_btn">+</button>
                    </div>
				    
			    </div>
				    
				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
@section('scripts')
	<script>
    
    $(document).ready(function(){
        $image=2;
	    $("#add_image_btn").click(function(e){
	        e.preventDefault();
	        $image2=$image-1;
	        $("#image_"+$image2).after('<div class="col-md-2" id="image_'+$image+'"><label class="control-label" for="image_'+$image+'">Image</label><input class="form-control" type="file" name="image[]"/></div>');
	        $image=$image+1;
	        return false;
	    });
	    
	});

	$(document).ready(function(){
        $packages=2;
	    $("#add_packages_btn").click(function(e){
	        e.preventDefault();
	        $packages2=$packages-1;
	        $("#packages_"+$packages2).after('<div class="col-md-6" id="packages_'+$packages+'"><label class="control-label" for="package_'+$packages+'">package</label><input class="form-control" type="text" name="package_name[]" placeholder="Package Title"/><input class="form-control" type="text" name="package_price[]" placeholder="Price"/><input class="form-control" type="text" name="package_description[]" placeholder="Description"/></div>');
	        $packages=$packages+1;
	        return false;
	    });
	    
	});

    
</script> 

@stop