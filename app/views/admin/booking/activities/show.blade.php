@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')
	
	<table>
		<tr>
			<td>Name
			</td>
			<td>{{$booking_activities->f_name}} {{$booking_activities->l_name}}
			</td>
		</tr>
		<tr>
			<td>Activity
			</td>
			<td>{{$activitiesplaces->name}}
			</td>
		</tr>
		<tr>
			<td>No of People
			</td>
			<td>{{$booking_activities->no_of_people}}
			</td>
		</tr>
		<tr>
			<td>Total Price
			</td>
			<td>{{$booking_activities->total_price}}
			</td>
		</tr>
		<tr>
			<td>Date
			</td>
			<td>{{$booking_activities->date}}
			</td>
		</tr>
		<tr>
			<td>Status
			</td>
			<td>{{$booking_activities->status}}
			</td>
		</tr>

		<tr><td colspan="2">Personal Details</td></tr>
		<tr>
			<td>email
			</td>
			<td>{{$booking_activities->email}}
			</td>
		</tr>
		<tr>
			<td>Phone
			</td>
			<td>{{$booking_activities->phone}}
			</td>
		</tr>
		<tr>
			<td>Address
			</td>
			<td>{{$booking_activities->location}}
			</td>
		</tr>
		
	</table>
	
@stop
	
@stop
