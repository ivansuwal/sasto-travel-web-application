@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

     {{-- Delete Post Form --}}
    <form id="deleteForm" class="form-horizontal" method="POST" action={{ URL::to('admin/booking/activities/' . $booking_activities->id . '/delete') }} autocomplete="off">
        
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="text" name="id" value="{{ $booking_activities->id }}" />
        <!-- <input type="hidden" name="_method" value="DELETE" /> -->
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                Delete Post
                <element class="btn-cancel close_popup">Cancel</element>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop