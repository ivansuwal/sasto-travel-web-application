@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

     {{-- Delete Post Form --}}
    <form id="deleteForm" class="form-horizontal" method="POST" action={{ URL::to('admin/booking/activities/' . $booking_activities->id . '/status') }} autocomplete="off">
        
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <select name="status">
            <option value="0" @if($booking_activities->status=="0") Selected @endif>Pending</option>
            <option value="1" @if($booking_activities->status=="1") Selected @endif>Processing</option>
            <option value="2" @if($booking_activities->status=="2") Selected @endif>Done</option>
        
        </select>
        <!-- <input type="hidden" name="_method" value="DELETE" /> -->
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                <element class="btn-cancel close_popup">Cancel</element>
                <button type="submit" class="btn btn-primary"> Change Status</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop