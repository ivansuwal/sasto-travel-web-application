@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

@section('keywords')Booking Activities @stop
@section('author')SastoTravel.com Activities @stop
@section('description')Activities @stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Activities Booking
		</h3>
	</div>

	<table id="activities" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">id</th>
				<th class="col-md-1">f_name</th>
				<th class="col-md-1">l_name</th>
				<th class="col-md-1">email</th>
				<th class="col-md-1">location</th>
				<th class="col-md-1">Phone</th>
				<th class="col-md-1">People</th>
				<th class="col-md-1">Date</th>
				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#activities').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/booking/activities/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop