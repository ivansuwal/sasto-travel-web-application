@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($booking_activities)){{ URL::to('admin/booking/activities/' . $booking_activities->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('f_name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="f_name">Activity Name</label>
						<input class="form-control" type="text" name="f_name" id="name" value="{{{ Input::old('f_name', isset($booking_activities) ? $booking_activities->f_name : null) }}}" />
						{{ $errors->first('f_name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div class="form-group {{{ $errors->has('l_name') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="l_name">l_name</label>
						<input class="form-control" type="textbox" name="l_name" id="l_name" value="{{{ Input::old('l_name', isset($booking_activities) ? $booking_activities->l_name : null) }}}" />
						{{ $errors->first('l_name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="email">email</label>
						<input class="form-control" type="textbox" name="email" id="email" value="{{{ Input::old('email', isset($booking_activities) ? $booking_activities->email : null) }}}" />
						{{ $errors->first('email', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="location">location</label>
						<input class="form-control" type="textbox" name="location" id="location" value="{{{ Input::old('location', isset($booking_activities) ? $booking_activities->location : null) }}}" />
						{{ $errors->first('location', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('phone') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="phone">phone</label>
						<input class="form-control" type="textbox" name="phone" id="phone" value="{{{ Input::old('phone', isset($booking_activities) ? $booking_activities->phone : null) }}}" />
						{{ $errors->first('phone', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('a_phone') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="a_phone">Activity</label>
						<select name="activitiesplaces_id">
							@foreach($activitiesplaces as $activity)
							<option value={{$activity->id}} {{{ isset($booking_activities) ? (($activity->id==$booking_activities->activitiesplaces_id)? "Selected" : null) : (Input::old('activitiesplaces_id')==$activity->id)? "Selected" :null }}}>
								{{ $activity->name }}
							</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('date') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="date">date</label>
						<input class="form-control" type="textbox" name="date" id="date" value="{{{ Input::old('date', isset($booking_activities) ? $booking_activities->date : null) }}}" />
						{{ $errors->first('date', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('no_of_people	') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="no_of_people">no_of_people</label>
						<input class="form-control" type="textbox" name="no_of_people" id="no_of_people" value="{{{ Input::old('no_of_people', isset($booking_activities) ? $booking_activities->no_of_people : null) }}}" />
						{{ $errors->first('no_of_people', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
