@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ $title }}} :: @parent
@stop

@section('keywords')Rent a Car @stop
@section('author')SastoTravel.com Rent a Car @stop
@section('description')Car Rental Servicec @stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Rent A Car Bookings
		</h3>
	</div>

	<table id="rentacar" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-1">id</th>
				<th class="col-md-1">person</th>
				<th class="col-md-1">Location</th>
				<th class="col-md-1">Car Type</th>
				<th class="col-md-1">Preferd</th>
				<th class="col-md-1">D-Date</th>
				<th class="col-md-1">A-Date</th>
				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#rentacar').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/booking/rentacar/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});
		});
	</script>
@stop