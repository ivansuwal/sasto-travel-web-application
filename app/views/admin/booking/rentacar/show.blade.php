@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	<table>
		<tr>
			<td>Pick up Point
			</td>
			<td>{{$booking_rentacar->location}}
			</td>
		</tr>
		<tr>
			<td>Type Of Car
			</td>
			<td>{{$typeofcar->name}}
			</td>
		</tr>
		<tr>
			<td>Prefered Car
			</td>
			<td>{{$booking_rentacar->prefered_car}}
			</td>
		</tr>
		<tr>
			<td>Departure Date
			</td>
			<td>{{$booking_rentacar->d_date}}
			</td>
		</tr>
		<tr>
			<td>Arrival Date
			</td>
			<td>{{$booking_rentacar->a_date}}
			</td>
		</tr>

		<tr><td colspan="2">Personal Details</td></tr>
		<tr>
			<td>Name
			</td>
			<td>{{$booking_person->f_name}}
			</td>
		</tr>
		<tr>
			<td>email
			</td>
			<td>{{$booking_person->email}}
			</td>
		</tr>
		<tr>
			<td>Phone
			</td>
			<td>{{$booking_person->phone}}/{{$booking_person->a_phone}}
			</td>
		</tr>
		<tr>
			<td>Address
			</td>
			<td>{{$booking_person->address}}
			</td>
		</tr>
		
	</table>
	
@stop
