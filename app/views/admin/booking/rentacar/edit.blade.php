@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
<form class="form-horizontal" method="post" action="@if (isset($booking_rentacar)){{ URL::to('admin/booking/rentacar/' . $booking_rentacar->id . '/edit') }}@endif"  autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		{{$booking_rentacar->id}}
		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('f_name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="f_name">Booking Person</label>
						<input class="form-control" type="text" name="f_name" id="f_name" value="{{{ Input::old('f_name', isset($booking_person) ? $booking_person->f_name : null) }}}" />
						{{ $errors->first('f_name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="email">email</label>
						<input class="form-control" type="textbox" name="email" id="email" value="{{{ Input::old('email', isset($booking_person) ? $booking_person->email : null) }}}" />
						{{ $errors->first('email', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('address') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="address">address</label>
						<input class="form-control" type="textbox" name="address" id="address" value="{{{ Input::old('address', isset($booking_person) ? $booking_person->address : null) }}}" />
						{{ $errors->first('address', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('phone') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="phone">phone</label>
						<input class="form-control" type="textbox" name="phone" id="phone" value="{{{ Input::old('phone', isset($booking_person) ? $booking_person->phone : null) }}}" />
						{{ $errors->first('phone', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('typeofcar_id') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="typeofcar_id">Type of car</label>
						<select name="typeofcar_id">
							@foreach($typeofcar as $car)
							<option value={{$car->id}} {{{ isset($booking_rentacar) ? (($car->id==$booking_rentacar->typeofcar_id)? "Selected" : null) : (Input::old('typeofcar_id')==$car->id)? "Selected" :null }}}>
								{{ $car->name }}
							</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('location') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="location">location</label>
						<input class="form-control" type="textbox" name="location" id="location" value="{{{ Input::old('location', isset($booking_rentacar) ? $booking_rentacar->location : null) }}}" />
						{{ $errors->first('location', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('prefered_car') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="prefered_car">prefered_car</label>
						<input class="form-control" type="textbox" name="prefered_car" id="prefered_car" value="{{{ Input::old('prefered_car', isset($booking_rentacar) ? $booking_rentacar->prefered_car : null) }}}" />
						{{ $errors->first('prefered_car', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('d_date') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="d_date">d_date</label>
						<input class="form-control" type="textbox" name="d_date" id="d_date" value="{{{ Input::old('d_date', isset($booking_rentacar) ? $booking_rentacar->d_date : null) }}}" />
						{{ $errors->first('d_date', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<div class="form-group {{{ $errors->has('a_date') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="a_date">a_date</label>
						<input class="form-control" type="textbox" name="a_date" id="a_date" value="{{{ Input::old('a_date', isset($booking_rentacar) ? $booking_rentacar->a_date : null) }}}" />
						{{ $errors->first('a_date', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
