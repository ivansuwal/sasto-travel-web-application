@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ String::title($hotel->name) }}} ::
@parent
@stop

{{-- Update the Meta Title --}}
@section('meta_title')
@parent

@stop

{{-- Update the Meta Description --}}
@section('meta_description')
<meta name="description" content="{{{ $hotel->name() }}}" />

@stop

{{-- Update the Meta Keywords --}}
@section('meta_keywords')
<meta name="keywords" content="{{{ $hotel->name() }}}" />

@stop

@section('meta_author')
<meta name="author" content="{{{ $hotel->name() }}}" />
@stop

{{-- Content --}}
@section('content')
<h3>{{ $hotel->name() }}</h3>

<p>{{ $hotel->name() }}</p>

<div>
	<span class="badge badge-info">Posted {{{ $hotel->name() }}}</span>
</div>

<hr />


@stop
