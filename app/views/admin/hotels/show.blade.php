@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')

	<table class="table-bordered">
		<tr>
			<td>Hotel Name
			</td>
			<td>{{$hotels->name}}
			</td>
		</tr>
		<tr>
			<td>Location
			</td>
			<td>{{$hotels->city}}
			</td>
		</tr>
		<tr>
			<td>Phone
			</td>
			<td>{{$hotels->phone}}
			</td>
		</tr>
		<tr>
			<td>Hotel facilities
			</td>
			<td>
				<table  class="table-bordered">
					@foreach($hotelsfacilities as $facilities)
					<tr>
						<td>
							{{$facilities->name}}		
						</td>
					</tr>
					@endforeach
				</table>
			</td>
		</tr>

		<tr>
			<td>Room types
			</td>
			<td>
				<table class="table-bordered">
					<tr>
						<td>
							Name
						</td>
						<td>
							Max Person
						</td>
						<td>
							Price
						</td>
						<td>
							NO of Rooms
						</td>
						<td>
							Available Rooms
						</td>
						<td>
							Room Facilities
						</td>
					</tr>
					@foreach($roomtypes as $type)
					<?php $i=0; ?>
					<tr>
						<td>
							{{$type->name}}
						</td>
						<td>
							{{$type->max_person}}
						</td>
						<td>
							{{$type->price}}
						</td>
						<td>
							{{$type->no_of_rooms}}
						</td>
						<td>
							{{$type->available_rooms}}
						</td>
						<td>
							@foreach($roomfacilities[$i] as $r)
								{{$r->name}}<br/>
							@endforeach
						</td>
					</tr>
					<?php $i=$i+1; ?>
					@endforeach
				</table>
			</td>
		</tr>

	</table>	
<div class="row">

	@foreach($hotelsimages as $image)
		<div class="col-md-2">
			<img src={{URL::to($image->image_path)}} width="180px">
		</div>
	@endforeach

</div>	


@stop