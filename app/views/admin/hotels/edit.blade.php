@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($hotels)){{ URL::to('admin/hotels/' . $hotels->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="city">Hotel Name</label>
						<input class="form-control" type="text" name="name" id="title" value="{{{ Input::old('name', isset($hotels) ? $hotels->name : null) }}}" />
						{{ $errors->first('city', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="city">City</label>
						<input class="form-control" type="text" name="city" id="city" value="{{{ Input::old('city', isset($hotels) ? $hotels->city : null) }}}" />
						{{ $errors->first('city', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="phone">Phone</label>
						<input class="form-control" type="text" name="phone" id="phone" value="{{{ Input::old('phone', isset($hotels) ? $hotels->phone : null) }}}" />
						{{ $errors->first('phone', '<span class="help-block">:message</span>') }}
					</div>
				</div>

				<!-- Images -->
				<div class="form-group">
					@foreach($hotelsimages as $image)
					<div class="col-md-12" id="eimage_{{$image->id}}">
                	    <input type="hidden" name="eimage_id[]" value="{{$image->id}}">
                	    <img src={{ URL::to($image->image_path) }} style="width:100px;height:100px;">
						<button type="button" class="btn btn-danger delete_image_btn"  data-image-id="{{$image->id}}" id="delete_image_btn">Delete</button>
                    </div>
                    @endforeach
				</div>
				<div class="form-group">
					<div class="col-md-2" id="image_1">
                	    <label class="control-label" for="image_1">Image</label>
						<input class="form-control" type="file" name="image[]"/>
				    	<button class="btn btn-success" id="add_image_btn">Add Another Image</button>
                    </div>
				    
			    </div>
				<!-- ./ images -->
				
				<!-- Hotel Facilities -->
				<div class="form-group">
					<div class="col-md-12">
                	    <label class="control-label" for="facilities">Hotel Facilities</label>
					</div>
					@foreach($amenities as $amenity)
						<div class="col-md-2">
							{{$amenity->name}}:<input class="form-control" type="checkbox" name="hotel_amenities[]" value="{{$amenity->id}}"
							@foreach($hotelsfacilities as $hf)
								@if($hf->amenities_id==$amenity->id)
								checked
								@endif
							@endforeach>
						</div>
					@endforeach
				</div>
				<!-- ./ Hotel Facilities -->
				
				<!-- Room types details-->
				@foreach($roomtypes as $roomtype)
				<div class="form-group">
					<div class="col-md-12" id="eroom_{{$roomtype->id}}">
                	    <label class="control-label" for="room_{{$roomtype->id}}">Room types</label>
						<input type="hidden" name="eroom_id[]" value="{{$roomtype->id}}"/>
						<input class="form-control" type="text" name="eroom_name[]" value="{{$roomtype->name}}" placeholder="Room Name"/>
						<input class="form-control" type="text" name="eroom_max_person[]" value="{{$roomtype->max_person}}" placeholder="Maximum person"/>
						<input class="form-control" type="text" name="eroom_price[]" value="{{$roomtype->price}}" placeholder="Price"/>
						<input class="form-control" type="text" name="eno_of_rooms[]" value="{{$roomtype->no_of_rooms}}" placeholder="Number of rooms"/>
						<input class="form-control" type="text" name="eavailable_rooms[]" value="{{$roomtype->available_rooms}}" placeholder="Available rooms"/>
						<div class="col-md-12">
						<!-- Room Amenities -->
						
							@foreach($amenities as $amenity)
								<div class="col-md-2">
									{{$amenity->name}}:<input class="form-control" type="checkbox" name="eroom_amenities_1[]" value="{{$amenity->id}}"
									@foreach($roomfacilities as $rf)
										@if(($rf->amenities_id==$amenity->id) && ($roomtype->id==$rf->room_type_id))
											checked
										@endif
									@endforeach>
								</div>
							@endforeach
						<!-- ./ Room amenities -->
						</div>	
						<button class="btn btn-danger delete_room_btn" data-room-id="{{$roomtype->id}}" id="delete_room_btn">Delete the Room</button>
                    </div>
				</div>
				@endforeach
				
				<div class="form-group">
					<div class="col-md-12" id="room_1">
                	    <label class="control-label" for="room_1">Room types</label>
						<input class="form-control" type="text" name="room_name[]" placeholder="Room Name"/>
						<input class="form-control" type="text" name="room_max_person[]" placeholder="Maximum person"/>
						<input class="form-control" type="text" name="room_price[]" placeholder="Price"/>
						<input class="form-control" type="text" name="no_of_rooms[]" placeholder="Number of rooms"/>
						<input class="form-control" type="text" name="available_rooms[]" placeholder="Available rooms"/>
						<div class="col-md-12">
						<!-- Room Amenities -->
						
							@foreach($amenities as $amenity)
								<div class="col-md-2">
									{{$amenity->name}}:<input class="form-control" type="checkbox" name="room_amenities_1[]" value="{{$amenity->id}}">
								</div>
							@endforeach
						<!-- ./ Room amenities -->
						</div>	
						<button class="btn btn-success" id="add_rooms_btn">Add a new Type of room</button>
                    </div>
				</div>
				<!-- ./ Room types details-->
				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop


@section('scripts')
	<script>
    
	    $(document).ready(function(){
	        $image=2;
		    $("#add_image_btn").click(function(e){
		        e.preventDefault();
		        $image2=$image-1;
		        $("#image_"+$image2).after('<div class="col-md-2" id="image_'+$image+'"><label class="control-label" for="image_'+$image+'">Image</label><input class="form-control" type="file" name="image[]"/></div>');
		        $image=$image+1;
		        return false;
		    });

    	    $(".delete_image_btn").click(function(e){
		        e.preventDefault();
		        $("#eimage_"+$(this).data("image-id")).remove(); 
		        return false;
		    });
    	    
    	    $(".delete_room_btn").click(function(e){
		        e.preventDefault();
		        $("#eroom_"+$(this).data("room-id")).remove(); 
		        return false;
		    });

		    
		});

		$(document).ready(function(){
	        $rooms=2;
		    $("#add_rooms_btn").click(function(e){
		        e.preventDefault();
		        $rooms2=$rooms-1;
		        $("#room_"+$rooms2).after('<div class="col-md-12" id="room_'+$rooms+'"><label class="control-label" for="room_'+$rooms+'">Room types</label><input class="form-control" type="text" name="room_name[]" placeholder="Room Name"/><input class="form-control" type="text" name="room_max_person[]" placeholder="Maximum person"/><input class="form-control" type="text" name="room_price[]" placeholder="Price"/><input class="form-control" type="text" name="no_of_rooms[]" placeholder="Number of rooms"/><input class="form-control" type="text" name="available_rooms[]" placeholder="Available rooms"/><div class="col-md-12">@foreach($amenities as $amenity)<div class="col-md-2">{{$amenity->name}}:<input class="form-control" type="checkbox" name="room_amenities_'+$rooms+'[]" value="{{$amenity->id}}"></div>@endforeach</div></div>');
		        $rooms=$rooms+1;
		        return false;
		    });
		    
		});

	</script> 

@stop