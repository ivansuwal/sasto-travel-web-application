@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($packages)){{ URL::to('admin/packages/' . $packages->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Package Title -->
				<div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="title">Package Title</label>
						<input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($packages) ? $packages->title : null) }}}" />
						{{ $errors->first('title', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package title -->

				<!-- Package Price -->
				<div class="form-group {{{ $errors->has('price') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="price">Package price</label>
						<input class="form-control" type="text" name="price" id="price" value="{{{ Input::old('price', isset($packages) ? $packages->price : null) }}}" />
						{{ $errors->first('price', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package price -->

				<!-- Package Offer -->
				<div class="form-group {{{ $errors->has('price_offer') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="price_offer">Package offer</label>
						<input class="form-control" type="text" name="price_offer" id="price_offer" value="{{{ Input::old('price_offer', isset($packages) ? $packages->price_offer : null) }}}" />
						{{ $errors->first('price_offer', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package Offer -->

				<!-- Package Vendor -->
				<div class="form-group {{{ $errors->has('vendor') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="vendor">Vendor</label>
						<input class="form-control" type="text" name="vendor" id="vendor" value="{{{ Input::old('vendor', isset($packages) ? $packages->vendor : null) }}}" />
						{{ $errors->first('vendor', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package vendor -->

				<!-- Package Start Date -->
				<div class="form-group {{{ $errors->has('start_date') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="start_date">Starting date</label>
						<input class="form-control" type="text" name="start_date" id="start_date" value="{{{ Input::old('start_date', isset($packages) ? $packages->start_date : null) }}}" />
						{{ $errors->first('start_date', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package start date -->

				<!-- Package End date -->
				<div class="form-group {{{ $errors->has('end_date') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="end_date">Ending date</label>
						<input class="form-control" type="text" name="end_date" id="end_date" value="{{{ Input::old('end_date', isset($packages) ? $packages->end_date : null) }}}" />
						{{ $errors->first('end_date', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ package end date -->

				
				<!-- Content -->
				<div id="div_description"class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="description">Description</label>
						<input class="form-control" type="textbox" name="description" id="description" value="{{{ Input::old('description', isset($packages) ? $packages->description : null) }}}" />
						{{ $errors->first('description', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<!--image-->
				<div class="form-group">
					<div class="col-md-12" id="eimage">
	                	    <img src={{ URL::to($packages->image_path) }} style="width:100px;height:100px;">
							<button type="button" class="btn btn-danger" id="delete_image_btn">Delete</button>
	                </div>
                </div>
                
				@foreach($itinerary as $i)
				<div id="eday_{{$i->day}}">
					<input type="hidden" name="ei_day_id[]" value="{{$i->id}}">
					<input type="hidden" name="ei_day[]" value="{{$i->day}}">
					Day {{$i->day}}:<input type="text" name="ei_day_description[]" value="{{$i->description}}">
				    <button class="btn btn-danger delete_day_btn" data-day="{{$i->day}}" id="delete_day_btn">x</button>
                </div>
				@endforeach
			    <div id="day_{{($i->day)+1}}">
			    	<input type="hidden" name="i_day[]" value="{{($i->day)+1}}">
			    	Day {{($i->day)+1}}:<input type="text" name="i_day_description[]">
			    </div>
        		<button class="btn btn-success" id="add_day_btn">Add New Day</button>
                <!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop

@section('scripts')
	<script>
    
    $(document).ready(function(){
        $day={{($i->day)+1}};
    $("#add_day_btn").click(function(e){
        e.preventDefault();
        $day2=$day-1;
        $("#day_"+$day2).after('<div id="day_'+$day+'"><input type="hidden" name="i_day[]" value="'+$day+'">Day '+$day+':<input type="text" name="i_day_description[]"></div>');
        $day=$day+1;
        return false;
    });

    $("#delete_image_btn").click(function(e){
	        e.preventDefault();
	        $("#eimage").remove();
	        $("#div_description").after('<div class="form-group"><div class="col-md-2" id="image"><label class="control-label" for="image">Add New Image</label><input class="form-control" type="file" name="image"/></div></div>');
				 
	        return false;
	    });
    
    $(".delete_day_btn").click(function(e){
	        e.preventDefault();
	        $("#eday_"+$(this).data("day")).remove(); 
	        return false;
	    });
    
});
    
</script> 

@stop
