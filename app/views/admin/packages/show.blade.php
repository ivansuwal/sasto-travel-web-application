@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')

	<table class="table-bordered">
		<tr>
			<td>Title
			</td>
			<td>{{$packages->title}}
			</td>
		</tr>
		<tr>
			<td>Price
			</td>
			<td>{{$packages->price}}
			</td>
		</tr>
		<tr>
			<td>Price Offer
			</td>
			<td>{{$packages->price_offer}}
			</td>
		</tr>

		<tr>
			<td>Start Date
			</td>
			<td>{{$packages->start_date}}
			</td>
		</tr>

		<tr>
			<td>end_date
			</td>
			<td>{{$packages->end_date}}
			</td>
		</tr>

		<tr>
			<td>Vendor
			</td>
			<td>{{$packages->vendor}}
			</td>
		</tr>

		<tr>
			<td>Description
			</td>
			<td>{{$packages->description}}
			</td>
		</tr>

		<tr>
			<td>Image
			</td>
			<td>{{$packages->image_path}}
			</td>
		</tr>

		<tr>
			<td>Itinerary
			</td>
			<td>
				<table>
					@foreach($itinerary as $i)
					<tr>
						<td>Day {{$i->day}}:
						</td>
						<td>{{$i->description}}
						</td>
					</tr>
					@endforeach
				</table>
			</td>
		</tr>

	</table>	
@stop