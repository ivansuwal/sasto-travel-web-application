@extends('admin.layouts.modal')


{{-- Content --}}
@section('content')

	<table  class="table-bordered">
		<tr>
			<td>Name
			</td>
			<td>{{$activities->name}}
			</td>
		</tr>
		<tr>
			<td>Description
			</td>
			<td>{{$activities->description}}
			</td>
		</tr>
		<tr>
			<td>Image
			</td>
			<td>
				<table>
					<tr>
						<td><img src={{ URL::to($activities->image_path) }} style="width:300px">
						</td>
					</tr>
				</table>
			</td>
		</tr>

		
	</table>	
@stop