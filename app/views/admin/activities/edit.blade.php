@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($activities)){{ URL::to('admin/activities/' . $activities->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Activity Name</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($activities) ? $activities->name : null) }}}" />
						{{ $errors->first('name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div id="div_description" class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="description">Description</label>
						<input class="form-control" type="textbox" name="description" id="description" value="{{{ Input::old('description', isset($activities) ? $activities->description : null) }}}" />
						{{ $errors->first('description', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<!--Image of the Activity--> 
				<div class="form-group">
					<div class="col-md-12" id="eimage">
	                	    <img src={{ URL::to($activities->image_path) }} style="width:100px;height:100px;">
							<button type="button" class="btn btn-danger" id="delete_image_btn">Delete</button>
	                </div>
                </div>
                
                <!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop


@section('scripts')
	<script>
    
	    $(document).ready(function(){
		    $("#delete_image_btn").click(function(e){
			        e.preventDefault();
			        $("#eimage").remove();
			        $("#div_description").after('<div class="form-group"><div class="col-md-2" id="image"><label class="control-label" for="image">Add New Image</label><input class="form-control" type="file" name="image"/></div></div>');
						 
			        return false;
			    });
	    
		});
	    
	</script> 

@stop
