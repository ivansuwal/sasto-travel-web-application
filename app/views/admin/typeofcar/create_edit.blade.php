@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($typeofcar)){{ URL::to('admin/typeofcar/' . $typeofcar->id . '/edit') }}@endif" enctype="multipart/form-data" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="name">Type Of Car</label>
						<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($typeofcar) ? $typeofcar->name : null) }}}" />
						{{ $errors->first('name', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Content -->
				<div class="form-group {{{ $errors->has('price') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="price">Price/Day</label>
						<input class="form-control" type="textbox" name="price" id="price" value="{{{ Input::old('price', isset($typeofcar) ? $typeofcar->price : null) }}}" />
						{{ $errors->first('price', '<span class="help-block">:message</span>') }}
					</div>
				</div>
				
				<div class="form-group {{{ $errors->has('seats') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="seats">Seats(ex. Driver)</label>
						<input class="form-control" type="textbox" name="seats" id="seats" value="{{{ Input::old('seats', isset($typeofcar) ? $typeofcar->seats : null) }}}" />
						{{ $errors->first('seats', '<span class="help-block">:message</span>') }}
					</div>
				</div>

				<!-- ./ content -->
			</div>
			<!-- ./ general tab -->

			<!-- Meta Data tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Cancel</element>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
