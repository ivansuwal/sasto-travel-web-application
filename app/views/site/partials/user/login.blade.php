<div class="container container_100">
    <div class="row">
        
        <div  class="signup_form col-md-4">
            <div class="row">
                <div class="col-md-12 signup_title">
                    <span>
                        LOGIN
                    </span>
                </div>
            </div>
            
            <div>
            <form class="form-horizontal" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                @if ( Session::get('error') )
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif

                @if ( Session::get('notice') )
                    <div class="alert">{{ Session::get('notice') }}</div>
                @endif

                <table>
                    <tr><td>    
                        <input class="signup_form_fname" tabindex="1" placeholder="{{ Lang::get('confide::confide.username_e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
                            </td></tr>

                    <tr><td>    
                            <input  class="signup_form_password" tabindex="2" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
                            </td></tr>
                    <tr>
                        <td>    
                            <div class="forgot-password"><a href="forgot">Forgot Password?</a></div>
                        </td>
                    </tr>

                    <tr><td>    
                           <input tabindex="3" type="submit">
                    </td></tr>  
                </table>
            </form>
        </div>
        
        <div class="row">
            <div class="signup_or_div">
                <div class="signup_or">
                    <span>OR</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="signup_login">
                    <span>LOGIN USING</span>
            </div>
        </div>
        <div class="row">
            <div class="signup_login">
                    <img src="../img/icon/fb.png">
                    <img src="../img/icon/gp.png">

            </div>
        </div>
        <div class="row">
            <div class="signup_or_div">
            </div>
        </div>
        <div class="row">
            <div class="signup_login">
                    <a href={{{ URL::to('user/create') }}}><span>DON'T HAVE AN ACCOUNT? SIGN UP HERE</span></a>
            </div>
        </div>
        
        
    </div>
    </div>

</div>
        


  