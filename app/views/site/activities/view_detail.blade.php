@extends('site.layouts.default')

@section('content')
		<div class="activitiesdetail">
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<div class="activitiesdetail-image">
						<img src={{ URL::to($activities->image_path) }} style="width:100%;">
			    	</div>
				</div>
				<div class="activitiesdetail-top_detail col-md-6 col-xs-12">
						<h1>{{$activitiesplaces->name}}</h1>
						<div class="col-md-12">
							<span>{{ String::tidy(Str::limit($activitiesplaces->description, 800)) }}</span>
						</div>
						<div class="col-md-12">
							<div class="activitiesdetail-top_detail-price col-md-6">
									<h2>Rs.{{$activitiesplaces->price }}/-</h2>
									<h4>Save 10%</h4>
							</div>
							<div class="activitiesdetail-top_detail-book col-md-6">
								<button>Book Now</button>
							</div>

						</div>
						
						
				</div>
			</div>
			
		<div class="row">
    <div class="col-md-2">
      <div class="col-md-12">
                  <div class="section_featured">
                      <h2><span>BROWSE</span></h2>
                  </div>
            </div>
      <div class="browse">
        @foreach($nonactivities as $non)
          <h2><a href={{URL::to('/activities/'.$non->slug)}}>{{$non->name}}</a></h2>
          
          @foreach($nonactivitiesplaces as $nonplaces)
            @if($non->id==$nonplaces->activities_id)
              <h3><a href={{URL::to('/activities/'.$non->slug.'/'.$nonplaces->slug)}}>-{{$nonplaces->name}}</a></h3>
              
            @endif
          @endforeach
        @endforeach
      </div>  
    </div>

    <div class="col-md-10" style="background-color:#FBFBFB;">
		      	<div class="col-md-12">
                  <div class="section_featured">
                      <h2><span>Packages</span></h2>
                  </div>
               	</div>
               	
<!--Packages div-->
			@foreach($activitiesplacespackages as $package)
               	<div class="packages_div col-md-12">
						<div class="packages_title col-md-4">
							<h2>{{$package->name}}</h2>
							<span>{{$package->description}}</span>
						</div>
						<div class="packages_price col-md-4">
						<h2>Rs. {{$package->price}}/-</h2>
						</div>
						<div class="packages_book col-md-4">
						<a href="{{URL::to('/book/activities/'.$activitiesplaces->slug.'/'.$package->id)}}">BOOK NOW</a>
						</div>

					</div>
			@endforeach
<!--/-Packages div-->
<!--Packages div-->
               	<div class="packages_div col-md-12">
						<div class="packages_title col-md-4">
							<h2>Package title</h2>
							<span>package description</span>
						</div>
						<div class="packages_price col-md-4">
						<h2>Rs. 28923/-</h2>
						</div>
						<div class="packages_book col-md-4">
						<button>BOOK NOW</button>
						</div>

					</div>
<!--/-Packages div-->
<!--Packages div-->
               	<div class="packages_div col-md-12">
						<div class="packages_title col-md-4">
							<h2>Package title</h2>
							<span>package description</span>
						</div>
						<div class="packages_price col-md-4">
						<h2>Rs. 28923/-</h2>
						</div>
						<div class="packages_book col-md-4">
						<button>BOOK NOW</button>
						</div>

					</div>
<!--/-Packages div-->


    	      	<div class="row">
    	      		<div class="col-md-6 col-xs-12">
    	      			<div id="map" class="activitiesdetail-map">
						</div>
					</div>
					<div class="col-md-6 col-xs-12">
    	      			<div class="activitiesdetail-map">
						<iframe style="height:inherit;width:inherit" src="https://www.youtube.com/embed/450p7goxZqg">
						</iframe>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-12">
					<img src={{ URL::to($activities->image_path) }} style="width:50%;">
			    	</div>
				</div>
		</div>
            </div>
      
	</div>
		</div>


@stop

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js"></script>
 <script>
      function initialize() {
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
          center: new google.maps.LatLng(27.665269, 85.429187),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@stop