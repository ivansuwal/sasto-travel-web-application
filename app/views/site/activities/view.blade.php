@extends('site.layouts.default')
{{-- Web site Title --}}
@section('title')
{{{ String::title($activities->name) }}} |
@parent
@stop

{{-- Update the Meta Title --}}
@section('meta_title')
{{ $activities->name }} |
@parent

@stop

{{-- Update the Meta Description --}}
@section('meta_description')
<meta name="description" content="{{{ $activities->name }}}" />

@stop

{{-- Update the Meta Keywords --}}
@section('meta_keywords')
<meta name="keywords" content="{{{ $activities->name }}}" />

@stop


{{-- content --}}
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div id="owl-example" class="owl-carousel">
	    <div class="item activities_carousel">
	        <img src={{ URL::to($activities->image_path) }}>
	        <div class="col-md-4 col-sm-6 activities_carousel-details">
	          <h1>{{ $activities->name }}</h1>
	          <span>
      			{{ String::tidy(Str::limit($activities->description, 600)) }}
	          </span>
	          </div>
	    </div>
	  </div>
	</div>
</div>


<!-- 
			<div class="row" style="position:relative;margin-left:1%;margin-right:1%;border-top:none;">
				<div class="col-md-3 col-sm-2 col-xs-12" >
				<div class="row"style="background-color:white;border:1px solid black;margin:10px;text-align:center;">
					<h3>Other Activities</h3>
					@foreach( $unselected_activities as $activity )
						<ul style="padding-left:0px;font-size:20px;">
							<a href={{{ URL::to('/activities/'.$activity->name) }}}>{{ $activity->name }}</a>
						</ul>
						
					@endforeach
				</div>
				</div>

				<div class="col-md-9 col-sm-10 col-xs-12">
				<div class="row" style="background-color:white;padding:10px;text-align:justify;border:1px solid black; margin:10px;">
					
					<h1>{{ $activities->name }}</h1>
					<p>{{ $activities->description }}</p>
				</div>
				</div>

			</div>
 -->

  <div class="row">
    <div class="col-md-2">
      <div class="col-md-12">
                  <div class="section_featured">
                      <h2><span>BROWSE</span></h2>
                  </div>
            </div>
      <div class="browse">
        @foreach($nonactivities as $non)
          <h2><a href={{URL::to('/activities/'.$non->slug)}}>{{$non->name}}</a></h2>
          
          @foreach($nonactivitiesplaces as $nonplaces)
            @if($non->id==$nonplaces->activities_id)
              <h3><a href={{URL::to('/activities/'.$non->slug.'/'.$nonplaces->slug)}}>-{{$nonplaces->name}}</a></h3>
              
            @endif
          @endforeach
        @endforeach
      </div>  
    </div>

    <div class="col-md-10" style="background-color:#FBFBFB;">
<!-- Top Activities Content-->
    
            
      <!--Top Activities Heading -->
            <div class="col-md-12">
                  <div class="section_featured">
                      <h2><span>{{ $activities->name }} Here</span></h2>
                  </div>
            </div>
      <!--/.Top Activities Heading -->
        
     
     <!--Top Activities Data Container-->
        <div class="col-md-12">
      @foreach($activitiesplaces as $places)
        <!--A single Row inside Container-->
     
            <div class="top_activities col-md-12 col-sm-6">
                  <div class="top_activities-content col-md-12">
          <!--Image of the top activity-->        
                          <div class="top_activities-img col-md-3">
                                  <img src="../img/intro-bg33.jpe">
                          </div>
          <!--/.Image of the top activity-->        
                          

                          <div class="top_activities-content-right col-md-9">
                <!--Activity Heading-->
                                <div class="top_activities-content-right-row row">
                                    <h2 style="margin-top:0px;">{{ $places->name }}</h2>
                                </div>
                <!--/.Activity Heading-->
                <!--Activity Description-->
                                <div class="top_activities-content-right-row row">
                                      {{ String::tidy(Str::limit($places->description, 600)) }}
                               </div>
                <!--/.Activity Description-->
                                <div class="top_activities-content-right-row row">
                                      <div class="col-md-6">
                 <!--Activity Price-->
                                           <div class="top_activities-content-right-row-price row">Rs. {{ $places->price }}/-</div>
                                            <div class="row">Save 10% </div>
                <!--/.Activity Price-->
                
                                      </div>
                <!--Activity Buttons-->
                
                                      <div class="top_activities-content-right-row col-md-6">
                                            <a href={{{ URL::to('/activities/'.$activities->slug.'/'.$places->slug) }}}>VIEW DETAILS</a>
                                            <a href={{{ URL::to('/book/activities/'.$places->slug) }}}>BOOK NOW</a>
                                      </div>
                <!--/.Activity Buttons-->
                                </div>
                          </div>
                  </div>
            </div>
    <!--/.A single Row inside Container-->
     
     @endforeach

        </div>
    <!-- /.container -->

    </div>
<!-- /.Top ACtivities section -->
</div>


@stop


@section('scripts')
    <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}"></script>

    
    <script>

    $(document).ready(function($) {
      $("#owl-example").owlCarousel({
      autoPlay: 10000, //Set AutoPlay to 3 seconds
 
      items : 1,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]});
    });


    
    </script>
    <script src="{{ asset('js/bootstrap-collapse.js') }}"></script>
    <script src="{{ asset('js/bootstrap-transition.js') }}"></script>

    <script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
    <script src="{{ asset('js/application.js') }}"></script>
@stop