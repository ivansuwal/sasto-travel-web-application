@extends('site.layouts.default')
@section('content')
<div class="container container_100">
    <div class="row">
        
        <div  class="signup_form col-md-4">
            <div class="row">
                <div class="col-md-12 signup_title">
                    <span>
                        {{ $book->name }}
                    </span>
                </div>
            </div>
            
            <div>
            <form class="form-horizontal" method="POST" action="{{ URL::to('book/activities/'.$book->slug.'/'.$packages->id) }}" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                @if ( Session::get('error') )
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif

                @if ( Session::get('notice') )
                    <div class="alert">{{ Session::get('notice') }}</div>
                @endif

                <table>
                    <tr>
                        <td>First Name:</td>
                        <td><input type="text" name="f_name" value="{{{ Input::old('f_name') }}}" required></td>
                    </tr>
                    <tr>
                        <td>last Name:</td>
                        <td><input type="text" name="l_name" value="{{{ Input::old('l_name') }}}" required></td>
                    </tr>
                    <tr>
                        <td>email:</td>
                        <td><input type="email" name="email" value="{{{ Input::old('email') }}}" required></td>
                    </tr>
                    <tr>
                        <td>location:</td>
                        <td><input type="text" name="location" value="{{{ Input::old('location') }}}" required></td>
                    </tr>
                    <tr>
                        <td>phone:</td>
                        <td><input type="number" name="phone" value="{{{ Input::old('phone') }}}" required></td>
                    </tr>
                    <tr>
                        <td>No of People</td>
                        <td><input type="number" name="no_of_people" value="{{{ Input::old('no_of_people') }}}" required></td>
                    </tr>
                    <tr>
                        <td>date:</td>
                        <td><input type="date" name="date" value="{{{ Input::old('date') }}}" required></td>
                    </tr>
                        
                    <tr><td>    
                           <input tabindex="3" type="submit">
                    </td></tr>  
                </table>
            </form>
        </div>
        
        
    </div>
    </div>

</div>
@stop        


  