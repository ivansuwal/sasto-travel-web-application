@extends('site.layouts.default')

@section('content')
  <div class="no_padding row">
  	<div class="col-md-2">
  		<div class="col-md-12">
                  <div class="section_featured">
                      <h2><span>BROWSE</span></h2>
                  </div>
            </div>
      <div class="browse">
        @foreach($nonpackages as $non)
          <h2><a href={{URL::to('/activities/'.$non->slug)}}>-{{$non->title}}</a></h2>
      	@endforeach
      </div>
  	</div>
<div class="col-md-10">
  	<div class="row">
		  <div class="col-md-12">
		    <div class="item packages_carousel">
		        <img src={{ URL::to($packages->image_path) }}>
		        <div class="col-md-4 col-sm-6 activities_carousel-details">
		          <h1>{{ $packages->title }}</h1>
		        </div>
		    </div>
		  
		  </div>
    </div>
      
      <div class="row packages_description">
        <div class="col-md-12">
          <h4>{{ $packages->title }}</h4>
        </div>
        <div class="col-md-12">
          {{ $packages->description }}
        </div>
        <div class="col-md-12">
            <div class="activitiesdetail-top_detail-price col-md-6">
                <h2>Rs.{{$packages->price }}/-</h2>
                <h4>Save 10%</h4>
            </div>
            <div class="activitiesdetail-top_detail-book col-md-6">
              <button>Book Now</button>
            </div>

        </div>
              
      </div>

      <div class="row">
        {{$packages->}}
      </div>

</div>
</div>
  

@stop
@section('scripts')
    <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}"></script>

    
    <script>

    $(document).ready(function($) {
      $("#owl-example").owlCarousel({
      autoPlay: 10000, //Set AutoPlay to 3 seconds
 
      items : 1,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]});
    });


    
    </script>
    <script src="{{ asset('js/bootstrap-collapse.js') }}"></script>
    <script src="{{ asset('js/bootstrap-transition.js') }}"></script>

    <script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
    <script src="{{ asset('js/application.js') }}"></script>
@stop