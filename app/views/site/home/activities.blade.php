@extends('site.layouts.default')

{{-- Content --}}
@section('content')
          <div class="row" style="padding-left:-400px !important;padding-right:0px !important;">
            <div class="col-md-12">
                <div class="activities_carousel-prev">
                        <img src="img/leftbutton.png">
                </div>
                <div class="activities_carousel-next">
                        <img src="img/rightbutton.png">
                </div>
                
              <div id="owl-example" class="owl-carousel">
                <div class="item activities_carousel">
                    <div class="col-md-4 com-sm-6 activities_carousel-details">
                      <h1>Rafting</h1>
                      <span>
                        Rafting and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                      </span>
                      <br/><br/>
                          <a href={{{ URL::to('/activities/rafting') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              
                <div class="item activities_carousel">
                    <img src="img/hr_bungy.jpg">
                    <div class="col-md-4 com-sm-6 activities_carousel-details">
                          <h1>Bungy</h1>
                          <span>
                            Bungy and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                          </span>
                          <br/><br/>
                          <a href={{{ URL::to('/activities/bungy') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              
                <div class="item activities_carousel">
                    <img src="img/hr_rafting.jpg">
                    <div class="col-md-4 com-sm-12 activities_carousel-details">
                      <h1>Rafting</h1>
                      <span>
                        Rafting and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                      </span>
                      <br/><br/>
                          <a href={{{ URL::to('/activities/rafting') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              
                <div class="item activities_carousel">
                    <img src="img/hr_bungy.jpg">
                    <div class="col-md-4 com-sm-12 activities_carousel-details">
                          <h1>Bungy</h1>
                          <span>
                            Bungy and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                          </span>
                          <br/><br/>
                          <a href={{{ URL::to('/activities/bungy') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              <div class="item activities_carousel">
                    <img src="img/hr_rafting.jpg">
                    <div class="col-md-4 com-sm-12 activities_carousel-details">
                      <h1>Rafting</h1>
                      <span>
                        Rafting and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                      </span>
                      <br/><br/>
                          <a href={{{ URL::to('/activities/rafting') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              
                <div class="item activities_carousel">
                    <img src="img/hr_bungy.jpg">
                    <div class="col-md-4 com-sm-12 activities_carousel-details">
                          <h1>Bungy</h1>
                          <span>
                            Bungy and white water rafting are recreational outdoor activities which use an inflatable raft to navigate a river or other body of water. This is often done on whitewater or different degrees of rough water, and generally represents a new and challenging environment for participants. Dealing with risk and the need for teamwork is often a part of the experience.
                          </span>
                          <br/><br/>
                          <a href={{{ URL::to('/activities/bungy') }}}>
                            View Details
                          </a>
                    </div>
                </div>
              </div>
            </div>
          </div>


          <!--     
    <div class="activities-tiles">
            <div class="row">
              <div class="activities-tiles-title col-md-12">
                <h2><span>ACTIVITIES</span></h2>
              </div>
            </div>
            <div class="row">
            <div class="col-md-12">

                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/r.jpe">
                    <div class="activities_detail">
                      <div class="row"><span>Rafting</span></div>
                      <a>View Details</a>
                    </div>
                </div>
              
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/tr.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Bungy</span></div>
                      <a>View Details</a>
                    </div>
                </div>
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/r.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
              
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/tr.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/pg.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/r.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
              
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/tr.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
              
                <div class="activities col-md-3 col-sm-12 col-xs-12">
                    <img src="img/pg.jpe">
                      <div class="activities_detail">
                      <div class="row"><span>Hikking</span></div>
                      <a>View Details</a>
                    </div>
                </div>
              
            </div>

    </div>
    </div>
    -->
    

    <div class="activities-tiles2">
          
        <!--<div class="row">
            <div class="activities_title">
              <span>ACTIVITIES</span>
            </div>
        </div>-->
        <div class="row">
            <div class="col-md-12">
                <div class="section_featured">
                    <h2><span>ACTIVITIES</span></h2>
                </div>
            </div>
        </div>
     

        <div class="row">
            <div class="col-md-12">
                @foreach ( $activities as $a)
                
                <div class="activities2 col-md-3 col-sm-6 col-xs-12">
                    <a href={{{ URL::to('/activities/'.$a->slug ) }}}>
                      <img src={{ $a->image_path}}>
                      <div class="activities_detail2">
                        <span>{{ $a->name }}</span>
                      </div>
                    </a>
                </div>
              
                @endforeach
                                
            </div>
        </div>
        
    </div>
    <!-- /.content-section-a -->
    
    

<!-- Top Activities Content-->
    <div class="content-section-a" style="background-color:#FBFBFB;">
            
      <!--Top Activities Heading -->
            <div class="row">
                  <div class="section_featured">
                      <h2><span>TOP ACTIVITIES</span></h2>
                  </div>
            </div>
      <!--/.Top Activities Heading -->
        
     
     <!--Top Activities Data Container-->
        <div class="container">

        <!--A single Row inside Container-->
     @foreach($activitiesplaces as $place)
            <div class="top_activities col-md-12 col-sm-6">
                  <div class="top_activities-content col-md-12">
          <!--Image of the top activity-->        
                          <div class="top_activities-img col-md-3">
                                  <img src="img/intro-bg33.jpe">
                          </div>
          <!--/.Image of the top activity-->        
                          

                          <div class="top_activities-content-right col-md-9">
                <!--Activity Heading-->
                                <div class="top_activities-content-right-row row">
                                    <h2 style="margin-top:0px;">{{ $place->name }}</h2>
                                </div>
                <!--/.Activity Heading-->
                <!--Activity Description-->
                                <div class="top_activities-content-right-row row">
                                       {{ String::tidy(Str::limit($place->description, 390)) }}
                               </div>
                <!--/.Activity Description-->
                                <div class="top_activities-content-right-row row">
                                      <div class="col-md-6">
                 <!--Activity Price-->
                                           <div class="top_activities-content-right-row-price row">Rs. {{ $place->price }}/-</div>
                                            <div class="row">Save 10% </div>
                <!--/.Activity Price-->
                
                                      </div>
                <!--Activity Buttons-->
                
                                      <div class="top_activities-content-right-row col-md-6">
                                            @foreach($activities as $a)
                                              @if($a->id==$place->activities_id)
                                                <a href={{{ URL::to('/activities/'.$a->slug.'/'.$place->slug) }}}>VIEW DETAILS</a>
                                              @endif
                                            @endforeach
                                            <a href={{{ URL::to('/book/activities/'.$place->slug) }}}>BOOK NOW</a>
                                      </div>
                <!--/.Activity Buttons-->
                                </div>
                          </div>
                  </div>
            </div>
    <!--/.A single Row inside Container-->
     
     @endforeach
        <!--A single Row inside Container-->
     
            <div class="top_activities col-md-12 col-sm-6">
                  <div class="top_activities-content col-md-12">
          <!--Image of the top activity-->        
                          <div class="top_activities-img col-md-3">
                                  <img src="img/intro-bg33.jpe">
                          </div>
          <!--/.Image of the top activity-->        
                          

                          <div class="top_activities-content-right col-md-9">
                <!--Activity Heading-->
                                <div class="top_activities-content-right-row row">
                                    <h2 style="margin-top:0px;">RBC Rafting</h2>
                                </div>
                <!--/.Activity Heading-->
                <!--Activity Description-->
                                <div class="top_activities-content-right-row row">
                                          or for any other reasons, you can use media queries to display different images on different devices.A large image can be perfect on a big computer screen, but useless on a small device. Why load a large image when you have to scale it down anyway? To reduce the load, or for any other reasons, you can use media queries to display different images on different devices.
                               </div>
                <!--/.Activity Description-->
                                <div class="top_activities-content-right-row row">
                                      <div class="col-md-6">
                 <!--Activity Price-->
                                           <div class="top_activities-content-right-row-price row">Rs. 10,000/-</div>
                                            <div class="row">Save 10% </div>
                <!--/.Activity Price-->
                
                                      </div>
                <!--Activity Buttons-->
                
                                      <div class="top_activities-content-right-row col-md-6">
                                            <a href="#">VIEW DETAILS</a>
                                            <a href="#">BOOK NOW</a>
                                      </div>
                <!--/.Activity Buttons-->
                                </div>
                          </div>
                  </div>
            </div>
    <!--/.A single Row inside Container-->
     
     <!--A single Row inside Container-->
     
            <div class="top_activities col-md-12 col-sm-6">
                  <div class="top_activities-content col-md-12">
          <!--Image of the top activity-->        
                          <div class="top_activities-img col-md-3">
                                  <img src="img/intro-bg33.jpe">
                          </div>
          <!--/.Image of the top activity-->        
                          

                          <div class="top_activities-content-right col-md-9">
                <!--Activity Heading-->
                                <div class="top_activities-content-right-row row">
                                    <h2 style="margin-top:0px;">RBC Rafting</h2>
                                </div>
                <!--/.Activity Heading-->
                <!--Activity Description-->
                                <div class="top_activities-content-right-row row">
                                          or for any other reasons, you can use media queries to display different images on different devices.A large image can be perfect on a big computer screen, but useless on a small device. Why load a large image when you have to scale it down anyway? To reduce the load, or for any other reasons, you can use media queries to display different images on different devices.
                               </div>
                <!--/.Activity Description-->
                                <div class="top_activities-content-right-row row">
                                      <div class="col-md-6">
                 <!--Activity Price-->
                                           <div class="top_activities-content-right-row-price row">Rs. 10,000/-</div>
                                            <div class="row">Save 10% </div>
                <!--/.Activity Price-->
                
                                      </div>
                <!--Activity Buttons-->
                
                                      <div class="top_activities-content-right-row col-md-6">
                                            <a href="#">VIEW DETAILS</a>
                                            <a href="#">BOOK NOW</a>
                                      </div>
                <!--/.Activity Buttons-->
                                </div>
                          </div>
                  </div>
            </div>
    <!--/.A single Row inside Container-->
     
     


        </div>
    <!-- /.container -->

    </div>
<!-- /.Top ACtivities section -->
    
    
    
     
    @stop
    
@section('scripts')
    <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}"></script>

    
    <script>

    $(document).ready(function($) {
      $("#owl-example").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        navigation : false,
        pagination : false,
        slideSpeed : 1500,
        rewindSpeed : 2000,
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1],
        itemsTablet : [768,1]});
        });
        $(".activities_carousel-next").click(function(){
                $("#owl-example").trigger('owl.next');
        });
        $(".activities_carousel-prev").click(function(){
                $("#owl-example").trigger('owl.prev');
        });

    
    </script>
    <script src="{{ asset('js/bootstrap-collapse.js') }}"></script>
    <script src="{{ asset('js/bootstrap-transition.js') }}"></script>

    <script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
    <script src="{{ asset('js/application.js') }}"></script>
@stop