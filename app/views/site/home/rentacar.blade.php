@extends('site.layouts.default')

{{-- Content --}}
@section('content')
<div class="rentacar">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="rentacar_banner">
                        <div class="rentacar_banner-detail col-md-4 col-xs-12">
                            <h4>HOW IT WORKS?</h4>
                            <span>
                                Letter of Recommendation Portal (LoRP) The ERAS Letter of Recommendation Portal ( LoRP ) is a tool designed to provide added flexibility for LoR Authors and reduce the scanning and uploading burden for medical schools. ... Effective ERAS 2016, all letters 
                            Letter of Recommendation Portal (LoRP) The ERAS Letter of Recommendation Portal ( LoRP ) is a tool designed to provide added flexibility for LoR Authors and reduce the scanning and uploading burden for medical schools. ... Effective ERAS 2016, all letters 
                            </span>
                        </div>
                    <div class="rentacar_banner-heading col-md-8 col-xs-12 hidden-xs">
                        <h1>RENT A CAR</h1>
                        <h5>RIDE YOUR DREAM</h5>
                    </div>        
                </div>
            </div>
        </div>
</div>
</div>

<div class="container container_100">
    <div class="row">
        
        <div  class="rentacar_form col-md-10">
            <div class="row">
                <div class="col-md-12 signup_title">
                    <span>
                        RENT A CAR NOW
                    </span>
                </div>
            </div>
            
            <div>
            <form  class="form-horizontal" method="POST" action="{{ URL::to('book/rentacar') }}" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                @if ( Session::get('error') )
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif

                @if ( Session::get('notice') )
                    <div class="alert">{{ Session::get('notice') }}</div>
                @endif

                <div class="row">
                <div class="col-md-6">
                <table>
                    
                    <tr>
                        <td>    
                            <h4>Fill the details</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <div class="label">Pick up Point</div>
                        </td>
                    </tr>
                    <tr><td>    
                        <input tabindex="1" placeholder="" type="text" name="location" id="location" value="{{ Input::old('location') }}" Required>
                            </td></tr>

                    <tr>
                        <td>    
                            <div class="label">Type of Car</div>
                        </td>

                    </tr>
                    <tr><td>    
                            <select name="typeofcar_id">
                                @foreach($typeofcar as $car)
                                <option value={{$car->id}} @if(Input::old('typeofcar_id')==$car->id) "Selected" @endif>{{$car->name}}</option>
                                @endforeach
                            </select></td></tr>
                    
                    <tr>
                        <td>    
                            <div class="label">Prefered Car</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="" type="text" name="prefered_car" id="prefered_car" value="{{ Input::old('prefered_car') }}">
                            </td>
                        </tr>
                    <tr>
                        <td>    
                            <div class="label">Departure Date</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="" type="date" name="d_date" id="d_date" value="{{ Input::old('d_date') }}" Required>
                            <input tabindex="2" placeholder="" type="time" name="d_time" id="d_time" value="{{ Input::old('d_time') }}">
                            </td>
                        </tr>
                    <tr>
                        <td>    
                            <div class="label">Arrival Date</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="" type="date" name="a_date" id="a_date" value="{{ Input::old('a_date') }}" Required>
                            <input tabindex="2" placeholder="" type="time" name="a_time" id="a_time" value="{{ Input::old('a_time') }}">
                            </td>
                        </tr>
                    <tr>
                        <td>    
                            <div class="label">Nationality</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <select name="nationality">
                                <option value="nepali" @if(Input::old('nationality')=='nepali') "Selected" @endif>Nepali</option>
                                <option value="foreigner" @if(Input::old('nationality')=='foreigner') "Selected" @endif>Foreigner</option>
                            </select>
                        </td>
                    </tr>
  
                </table>
                </div>
                <div class="col-md-6">
                <table>
                    <tr>
                        <td>    
                            <h4>Personal Details</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <div class="label">Full Name</div>
                        </td>
                    </tr>
                    <tr><td>    
                        <input tabindex="1" placeholder="" type="text" name="f_name" id="f_name" value="{{ Input::old('f_name') }}" Required>
                            </td></tr>

                    <tr>
                        <td>    
                            <div class="label">email</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="abc@example.com" type="email" name="email" id="email" value="{{ Input::old('email') }}" Required>
                            </td></tr>
                    <tr>
                        <td>    
                            <div class="label">Phone Number</div>
                        </td>
                    </tr>
                    <tr><td>    
                        <input tabindex="1" placeholder="" type="number" name="phone" id="phone" value="{{ Input::old('phone') }}" Required>
                            </td></tr>

                    <tr>
                        <td>    
                            <div class="label">Alternate Phone Number</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="" type="number" name="a_phone" id="a_phone" value="{{ Input::old('a_phone') }}" Required>
                            </td></tr>
                    
                    <tr>
                        <td>    
                            <div class="label">Address</div>
                        </td>
                    </tr>
                    <tr><td>    
                            <input tabindex="2" placeholder="" type="text" name="address" id="address" value="{{ Input::old('address') }}" Required>
                            </td></tr>
                    
                    <tr><td>    
                           <input tabindex="3" type="submit" value="RENT A CAR">
                    </td></tr>  
                </table>
                </div>
            </div>
            </form>
        </div>
        
        
        
    </div>
    </div>

</div>
        


  
@stop
