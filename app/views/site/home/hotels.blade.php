@extends('site.layouts.default')

{{-- Content --}}
@section('content')
<div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="intro-message">
                        <h3>Hotels</h3>
                        <hr class="intro-divider">
                         <ul class="list-inline intro-social-buttons">
                            <div class="row">
                                <input type="radio" name="flight_type" value="one_way">One Way
                                <input type="radio" name="flight_type" value="round_trip">Round Trip
                            </div>
                            <input type="text" placeholder="FROM"/>
                            <input type="text" placeholder="TO"/>
                            <input type="date" placeholder="DATE"/>
                            <br/>
                            <br/>
                            <li>
                                
                                <a href="#" class="btn btn-default btn-md"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Search</span></a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Trending Content -->
    
    <div class="content-section-a">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="section_featured">
                    <h2><span>TRENDING</span></h2>
                </div>
            </div>
            <div class="col-md-2 col-sm-12 pre_next_buttons">
                <button><span>PREV</span></button>
                <button>NEXT</button>
                
            </div>
        </div>
        
    
        <div class="container">
            <div class="row trending">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <img src="img/r.jpe">
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK NOW</button>
                    </div>
                </div>
              
                <div class="col-md-4  col-sm-12 col-xs-12">
                    <img src="img/tr.jpe">
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <img src="img/pg.jpe">
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
    
    <!-- Recommendation Content-->
     <div class="content-section-a">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="section_featured">
                    <h2><span>RECOMMENDATIONS</span></h2>
                </div>
            </div>
            <div class="col-md-2 pre_next_buttons">
                <button><span>PREV</span></button>
                <button>NEXT</button>
                
            </div>
        </div>
        
    
        <div class="container">
            <div class="row trending">
                <div class="col-md-4 col-xs-12">
                    <img src="img/intro-bg33.jpe" >
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
                <div class="col-md-4 col-xs-12">
                    <img src="img/intro-bg11.jpe">
                        <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
                <div class="col-md-4 col-xs-12">
                    <img src="img/hk.jpe">
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
    
    
    <!--Deals of the week Content-->
    <div class="content-section-a">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="section_featured">
                    <h2><span>DEALS OF THE WEEK</span></h2>
                </div>
            </div>
            <div class="col-md-2 pre_next_buttons">
                <button><span>PREV</span></button>
                <button>NEXT</button>
                
            </div>
        </div>
        
    
        <div class="container">
            <div class="row trending">
                <div class="col-md-4 col-xs-12">
                    <img src="img/intro-bg33.jpe" >
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
                <div class="col-md-4 col-xs-12">
                    <img src="img/intro-bg11.jpe" >
                        <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
                <div class="col-md-4 col-xs-12">
                    <img src="img/into-bg22.jpe">
                    <div class="trending_des">
                        <p>Name of the place</p>
                        <button>BOOK</button>
                    </div>
                </div>
              
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

@stop
