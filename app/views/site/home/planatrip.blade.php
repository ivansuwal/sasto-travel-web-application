@extends('site.layouts.default')

{{-- Content --}}
@section('content')
<div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-xs-12">
                        <div class="row">
                            <div class="activities_title">
                                    <a href={{URL::to('/planatrip/create')}}><h3><span>START PLANNING A TRIP</span></h3></a>
                            </div>
                        </div>
                      
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Trending Content -->
    
    <div class="content-section-a">
        
    
        <div class="container">
            <div class="row">
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    

    <!-- /.content-section-a -->
            <div class="container"> 
                    <div class="row">
                            <div class="planatrip_left col-md-6 col-sm-6 col-xs-12">
                                <img src="img/planatrip2.jpg" width="80%">
                                <div class="planatrip_left-details">

                                </div>
                            </div>

                            <div class="planatrip_right col-md-6 col-sm-6 col-xs-12">
                                <form>
                                    <label>Destination</label>
                                    <input type="text" name="f_name">

                                    <label>Route</label>
                                    <input type="text" name="f_name">

                                    <label>Date</label>
                                    <input type="date" name="f_name">

                                    <label>Date</label>
                                    <input type="date" name="f_name">

                                    <label>Details</label>
                                    <input type="text" name="f_name">

                                    <label>Adults</label>
                                    <input type="number" name="f_name">
                                    
                                    <label>Children</label>
                                    <input type="number" name="f_name">
                                    
                                    <label>Infants</label>
                                    <input type="number" name="f_name">
                                    
                                    <label>Prefered Hotel</label>
                                    <input type="text" name="f_name">

                                    <label>Hotel Details</label>
                                    <input type="textbox" name="f_name">

                                    <label>Address</label>
                                    <input type="text" name="f_name">

                                    <label>Phone</label>
                                    <input type="text" name="f_name">

                                    <label>Alternate phone</label>
                                    <input type="text" name="f_name">

                                    <label>email</label>
                                    <input type="text" name="f_name">


                                </form>
                            </div>
                            
                    </div>
            </div>   
    <!-- /.content-section-a -->


    <div class="activities-tiles2">
          
        <div class="row">
            <div class="col-md-12">
                <div class="section_featured">
                    <h2><span>TOO LAZY TO PLAN</span></h2>
                </div>
            </div>
        </div>
     

        <div class="row">
            <div class="col-md-12">
                @foreach ( $activities as $a)
                <div class="activities2 col-md-3 col-sm-6 col-xs-12">
                    <a href={{{ URL::to('/activities/'.$a->slug ) }}}>
                      <img src={{ $a->image_path}}>
                      <div class="activities_detail2">
                        <span>{{ $a->name }}</span>
                    </div>
                    </a>
                </div>
              
                @endforeach
                                
            </div>
        </div>
        
    </div>
@stop
