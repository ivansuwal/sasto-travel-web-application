<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			SastoTravel.com
			@show
		</title>
		@section('meta_keywords')
		<meta name="keywords" content="your, awesome, keywords, here" />
		@show
		@section('meta_author')
		<meta name="author" content="Jon Doe" />
		@show
		@section('meta_description')
		<meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei." />
                @show
		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- CSS
		================================================== -->
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">

	    <!-- Custom CSS -->
	    <link href="{{ asset('css/landing-page.css') }}" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

	    <!-- Owl Carousel Assets -->
        <link href="{{ asset('owl-carousel/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('owl-carousel/owl.theme.css') }}" rel="stylesheet">

        <!-- Prettify -->
        <link href="{{ asset('js/google-code-prettify/prettify.css') }}" rel="stylesheet">
		

		<script src="{{ asset('js/bootstrap-collapse.js') }}"></script>
    	<script src="{{ asset('js/bootstrap-transition.js') }}"></script>

        <script src="{{ asset('js/google-code-prettify/prettify.js') }}"></script>
        <script src="{{ asset('js/bootbox.js') }}"></script>
        <script src="{{ asset('js/application.js') }}"></script>
        <!--  jQuery 1.7+  -->
		<script src="http://owlgraphic.com/owlcarousel/assets/js/jquery-1.9.1.min.js"></script>
		 
		<!-- Include js plugin -->
		<script src="http://owlgraphic.com/owlcarousel/owl-carousel/owl.carousel.js"></script>
		
        

        <style>
        body {
            padding: 0;
            background-color:#FBFBFB;
        }
		@section('styles')
		@show
		</style>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
	</head>

	<body>
		<!-- To make sticky footer need to wrap in a div -->
			<div class="logo_banner">
				<img src={{ asset('img/st_logo.png') }}>
            </div>
		<!-- Navbar -->
		<div class="navbar navbar-default " id="navbar">
			 <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
						<li {{ (Request::is('/') ? ' class="active"' : '') }}><a href="{{{ URL::to('/') }}}">Flights</a></li>
						<li {{ (Request::is('hotels') ? ' class="active"' : '') }}><a href="{{{ URL::to('/hotels') }}}">Hotels</a></li>
						<li {{ (Request::is('activities')||Request::is('activities/*') ? ' class="active"' : '') }}><a href="{{{ URL::to('/activities') }}}">Activities</a></li>
						<li {{ (Request::is('planatrip') ? ' class="active"' : '') }}><a href="{{{ URL::to('/planatrip') }}}">Plan a Trip</a></li>
						<li {{ (Request::is('rentacar') ? ' class="active"' : '') }}><a href="{{{ URL::to('/rentacar') }}}">Rent a Car</a></li>
						
					</ul>
					<ul class="nav navbar-nav pull-right">
                        @if (Auth::check())
                        @if (Auth::user()->hasRole('admin'))
                        <li><a href="{{{ URL::to('admin') }}}">Admin Panel</a></li>
                        @endif
                        <li><a href="{{{ URL::to('user') }}}">Logged in as {{{ Auth::user()->username }}}</a></li>
                        <li><a href="{{{ URL::to('user/logout') }}}">Logout</a></li>
                        @else
                        <li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/login') }}}">Login</a></li>
                        <li {{ (Request::is('user/create') ? ' class="active"' : '') }}><a href="{{{ URL::to('user/create') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>
                        @endif
                    </ul>
					<!-- ./ nav-collapse -->
				</div>
			</div>
		</div>
		<!-- ./ navbar -->

			<!-- Notifications -->
			@include('notifications')
			<!-- ./ notifications -->

			<!-- Content -->
			@yield('content')
			<!-- ./ content -->
		
		<!-- the following div is needed to make a sticky footer -->
		<div id="push"></div>
		

	    <!-- Footer -->
    <footer>
        <div>
            <div>
                <div class="footer_block col-md-3 col-xs-6">
                    <div class="footer_title">Site Map</div>
                        <p>
                            <a href="#">Flights</a>
                        <br/>
                            <a href="#about">Accomodation</a>
                        <br/>
                            <a href="#services">Rental</a>
                        <br/>
                            <a href="#contact">Activities</a>
                        </p>
                </div>
                <div class="footer_block col-md-3 col-xs-6">
                    <div class="footer_title">Contact Us</div>
                    
                        <p>
                           info@sastotravel.com
                            <br/>
                            01-4412423/01-4412423
                            <br/>
                            <a href="#services">facebook.com/sastotravel</a>
                            <br/>
                            <a href="#contact">instagram.com/sastotravel</a>
                        </p>
                    </ul>
                    
                </div>
                <div class="footer_block col-md-3 col-xs-6">
                    <div class="footer_title">Others</div>
                        <p>
                           Frequently asked Questions
                            <br/>
                            Terms and Conditions
                            <br/>
                            Disclaimer Policy
                        </p>
                    </ul>
                </div>
                <div class="footer_block col-md-3 col-xs-6">
                    <div class="footer_title"><h3></h3></div>
                        <p>
                          Sponsors
                        </p>
                    
                </div>
                
                
            </div>
            
        </div>
        <div class="container" id="apple">
        <div class="col-md-12">
                <p class="copyright text-muted small">Copyright &copy; SastoTravel.com 2015. All Rights Reserved</p>
        </div>
        </div>
        
    </footer>
    

    <!-- Javascripts
		================================================== -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        <script>
        $("#apple").on("click", ".alert", function(e) {
            bootbox.alert("Hello world!", function() {
                console.log("Alert Callback");
            });
        });
        </script>
        @yield('scripts')
	</body>
</html>
