<?php

use Illuminate\Support\Facades\URL;

class Typeofcar extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="type_of_car";
	public function delete()
	{
		
		// Delete the blog post
		return parent::delete();
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
