<?php

use Illuminate\Support\Facades\URL;

class Hotelsimages extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="hotels_images";
	public function delete()
	{
		// Delete the image_path
		return parent::delete();
	}

	public function hotels()
	{
		return $this->belongsTo('hotels');
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
