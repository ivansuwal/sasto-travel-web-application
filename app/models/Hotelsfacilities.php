<?php

use Illuminate\Support\Facades\URL;

class Hotelsfacilities extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="hotels_facilities";
	public function delete()
	{
		// Delete the room facilities
		return parent::delete();
	}

	
	public function hotels()
	{
		return $this->belongsTo('Hotels');
	}


	public function url()
	{
		return Url::to($this->name);
	}


}
