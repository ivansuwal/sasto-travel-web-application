<?php

use Illuminate\Support\Facades\URL;

class Activities extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		// Delete the blog post
		return parent::delete();
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
