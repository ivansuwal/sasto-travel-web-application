<?php

use Illuminate\Support\Facades\URL;

class Itinerary extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="itinerary";
	public function delete()
	{
		// Delete the Itinerary
		return parent::delete();
	}

	public function packages()
	{
		return $this->belongsTo('Packages');
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
