<?php

use Illuminate\Support\Facades\URL;

class Amenities extends Eloquent {

	
	public function delete()
	{
		// Delete the amenities
		return parent::delete();
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
