<?php

use Illuminate\Support\Facades\URL;

class Activitiesplaces extends Eloquent {

	
	protected $table="activitiesplaces";
	
	public function delete()
	{
		// Delete the packages related to the activity Place
		$this->activitiesplacespackages()->delete();
		
		// Delete the images related to the activity Place
		$this->activitiesplacesimages()->delete();
		

		// Delete the blog post
		return parent::delete();
	}

	
	public function activitiesplacesimages()
	{
		return $this->hasMany('Activitiesplacesimages');
	}

	public function activitiesplacespackages()
	{
		return $this->hasMany('Activitiesplacespackages');
	}
	
	public function url()
	{
		return Url::to($this->name);
	}


}
