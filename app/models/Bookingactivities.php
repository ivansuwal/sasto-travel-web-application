<?php

use Illuminate\Support\Facades\URL;

class Bookingactivities extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="booking_activities";
	public function delete()
	{
		// Delete the comments
		$this->comments()->delete();

		// Delete the blog post
		return parent::delete();
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
