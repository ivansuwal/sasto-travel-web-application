<?php

use Illuminate\Support\Facades\URL;

class BookingRentacar extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="booking_rentacar";
	public function delete()
	{
		// Delete the Personal Info of booker
		$this->bookingperson()->delete();

		// Delete the Booking
		return parent::delete();
	}

	/**
	 * Get the Personal Info of the person.
	 *
	 * @return array
	 */
	public function bookingperson()
	{
		return $this->has('BookingPerson');
	}

	public function url()
	{
		return Url::to($this->name);
	}


}
