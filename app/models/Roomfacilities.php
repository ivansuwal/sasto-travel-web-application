<?php

use Illuminate\Support\Facades\URL;

class Roomfacilities extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="room_facilities";
	public function delete()
	{
		// Delete the room facilities
		return parent::delete();
	}

	
	public function roomtypes()
	{
		return $this->belongsTo('Roomfacilities');
	}


	public function url()
	{
		return Url::to($this->name);
	}


}
