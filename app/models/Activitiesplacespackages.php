<?php

use Illuminate\Support\Facades\URL;

class Activitiesplacespackages extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="activitiesplaces_packages";
	public function delete()
	{
		// Delete the blog post
		return parent::delete();
	}

	
	public function activitiesplaces()
	{
		return $this->belongsTo('activitiesplaces');
	}


	public function url()
	{
		return Url::to($this->name);
	}


}
