<?php

use Illuminate\Support\Facades\URL;

class BookingPerson extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="booking_person";
	
	public function delete()
	{
		// Delete the Personal Info
		return parent::delete();
	}

	
	public function bookingrentcar()
	{
		return $this->belongsTo('BookingRentacar');
	}

	public function url()
	{
		return Url::to($this->name);
	}


}
