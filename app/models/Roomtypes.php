<?php

use Illuminate\Support\Facades\URL;

class Roomtypes extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="room_type";
	public function delete()
	{
		// Delete the comments
		$this->roomfacilities()->delete();

		// Delete the blog post
		return parent::delete();
	}

	
	public function roomfacilities()
	{
		return $this->hasMany('Roomfacilities','room_type_id');
	}


	public function hotels()
	{
		return $this->belongsTo('Hotels');
	}


	public function url()
	{
		return Url::to($this->name);
	}


}
