<?php

use Illuminate\Support\Facades\URL;

class Hotels extends Eloquent {

	/*
	Table name
	*/
	protected $table="hotels";
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		// Delete the images
		$this->hotelsimages()->delete();
		
		// Delete the roomtypes
		$this->roomtypes()->delete();


		// Delete the blog post
		return parent::delete();
	}

	public function hotelsimages()
	{
		return $this->hasMany('Hotelsimages');
	}

	public function roomtypes()
	{
		return $this->hasMany('Roomtypes');
	}

	
	public function url()
	{
		return Url::to($this->slug);
	}


}
