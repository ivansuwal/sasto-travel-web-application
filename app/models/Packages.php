<?php

use Illuminate\Support\Facades\URL;

class Packages extends Eloquent {

	
	/*
	Table name
	*/
	protected $table="packages";
	
	/**
	 * Deletes a package and all
	 * the associated itinerary.
	 *
	 * @return bool
	 */
	
	
	public function delete()
	{
		// Delete the comments
		$this->itinerary()->delete();

		// Delete the blog post
		return parent::delete();
	}

	public function itinerary()
	{
		return $this->hasMany('Itinerary');
	}

	public function url()
	{
		return Url::to($this->name);
	}


}
