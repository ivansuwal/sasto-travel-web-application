<?php

use Illuminate\Support\Facades\URL;

class Activitiesplacesimages extends Eloquent {

	
	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	protected $table="activitiesplaces_images";
	public function delete()
	{
		// Delete the image_path
		return parent::delete();
	}

	public function activitiesplaces()
	{
		return $this->belongsTo('activitiesplaces');
	}

	
	public function url()
	{
		return Url::to($this->name);
	}


}
