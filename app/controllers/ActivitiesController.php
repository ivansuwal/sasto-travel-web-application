<?php

class ActivitiesController extends BaseController {

    /**
     * Post Model
     * @var Post
     */
    /**
     * User Model
     * @var User
     */
    protected $user;
    protected $activities;
    protected $activitiesplaces;
    protected $activitiesplacesimages;
    protected $activitiesplacespackages;
    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(User $user,Activities $activities,Activitiesplaces $activitiesplaces,Activitiesplacesimages $activitiesplacesimages,Activitiesplacespackages $activitiesplacespackages)
    {
        parent::__construct();

        $this->user = $user;
        $this->activities = $activities;
		$this->activitiesplaces = $activitiesplaces;
		$this->activitiesplacesimages = $activitiesplacesimages;
		$this->activitiesplacespackages = $activitiesplacespackages;

    }
    
	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getView($activity)
	{
		$activities = $this->activities->where('slug', '=', $activity)->first();
		$nonactivities = $this->activities->where('slug', '<>', $activity)->get();
		
		$activitiesplaces = $this->activitiesplaces->where('activities_id', '=', $activities->id)->get();
		$nonactivitiesplaces = $this->activitiesplaces->where('activities_id', '<>', $activities->id)->get();
		
		$unselected_activities=$this->activities->where('name', '<>', $activity)->get();
		// Check if the blog post exists
		if (is_null($activities))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		// Show the page
		return View::make('site/activities/view', compact('activities','nonactivities','activitiesplaces','nonactivitiesplaces','unselected_activities'));
	}

	public function getViewDetail($activity,$activitydetail)
	{
		$activities = $this->activities->where('slug', '=' ,$activity)->first();
		$activitiesplaces = $this->activitiesplaces->where('slug', '=' ,$activitydetail)->first();
		$nonactivities = $this->activities->where('slug', '<>', $activity)->get();
		$nonactivitiesplaces = $this->activitiesplaces->where('activities_id', '<>', $activities->id)->get();
		
		//Image of the place
		$activitiesplacesimages=$this->activitiesplacesimages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
		
		//Packages provided by the activity place
		$activitiesplacespackages=$this->activitiesplacespackages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
		// Check if the blog post exists
		if (is_null($activitiesplaces))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		// Show the page
		return View::make('site/activities/view_detail', compact('activities','nonactivities','activitiesplaces','activitiesplacespackages','activitiesplacesimages','nonactivitiesplaces','unselected_activities'));
	}


	

	public function getData()
    {
        $activities = Activities::select(array('activities.id', 'activities.name','activities.description'));

        return Datatables::of($activities)        
        ->make();
    }	
}
