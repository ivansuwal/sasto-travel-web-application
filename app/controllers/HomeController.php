<?php

class HomeController extends BaseController {

    /**
     * Activities Model
     * @var Activities
     */
    
    protected $activities;

    /**
     * User Model
     * @var User
     */
    protected $user;
	protected $activitiesplaces;
	protected $typeofcar;
    
    /**
     * Inject the models.
     * @param Activities $activities
     * @param User $user
     */
    public function __construct(Typeofcar $typeofcar,Post $post, User $user,Activities $activities,Activitiesplaces $activitiesplaces)
    {
        parent::__construct();

        $this->post = $post;
        $this->activities = $activities;
        $this->activitiesplaces = $activitiesplaces;
        $this->typeofcar = $typeofcar;
		
        $this->user = $user;
    }
    
	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getIndex()
	{
		// Get all the blog posts
		//$posts = $this->post->orderBy('created_at', 'DESC')->paginate(10);

		// Show the page
		/*$client = new \Guzzle\Service\Client('http://api.github.com/users/');
		$array = array('from' => 'KTM',
				'to' => 'PKR',
				'trip_type' => 'R',
			  	'departure' => '21­07­2015',
			  	'return' => '22­07­2015',
			  	'adult' => 2,
			  	'children' => 1
			);
		$response = $client->get($data)->send();
		      $username = "username_provided_by_trasync";
		      $password = "password_provided_by_trasync";
		      $url = 'http://api.trasync.com/search';
		      $curl = curl_init($url);
		      curl_setopt($curl, CURLOPT_URL, $url);
		      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		      curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
		      curl_setopt($curl, CURLOPT_POST, true);
		      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		      $result = curl_exec($curl);
		      curl_close($curl);
		*/return View::make('site/home/index');
	}

	public function getHotels()
	{
		// Get all the blog posts
		//$posts = $this->post->orderBy('created_at', 'DESC')->paginate(10);

		// Show the page
		return View::make('site/home/hotels');
	}
	public function getActivities()
	{
		// Get all the activtities
		$activities = $this->activities->get();
		// Get all the activtities places
		$activitiesplaces = $this->activitiesplaces->get();


		// Show the page
		return View::make('site/home/activities',compact('activities','activitiesplaces'));
	}
	public function getPlanatrip()
	{
		// Get all the activtities
		
		$activities = $this->activities->get();
		return View::make('site/home/planatrip',compact('activities','typeofcars'));
	}
	public function getRentacar()
	{
		// Get all the blog posts
		$typeofcar = $this->typeofcar->get();
		return View::make('site/home/rentacar',compact('typeofcar'));
		}


	/**
	 * View a blog post.
	 *
	 * @param  string  $slug
	 * @return View
	 * @throws NotFoundHttpException
	 */
	public function getView($slug)
	{
		// Get this blog post data
		$post = $this->post->where('slug', '=', $slug)->first();

		// Check if the blog post exists
		if (is_null($post))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}

		// Get this post comments
		$comments = $post->comments()->orderBy('created_at', 'ASC')->get();

        // Get current user and check permission
        $user = $this->user->currentUser();
        $canComment = false;
        if(!empty($user)) {
            $canComment = $user->can('post_comment');
        }

		// Show the page
		return View::make('site/blog/view_post', compact('post', 'comments', 'canComment'));
	}

	/**
	 * View a blog post.
	 *
	 * @param  string  $slug
	 * @return Redirect
	 */
	public function postView($slug)
	{

        $user = $this->user->currentUser();
        $canComment = $user->can('post_comment');
		if ( ! $canComment)
		{
			return Redirect::to($slug . '#comments')->with('error', 'You need to be logged in to post comments!');
		}

		// Get this blog post data
		$post = $this->post->where('slug', '=', $slug)->first();

		// Declare the rules for the form validation
		$rules = array(
			'comment' => 'required|min:3'
		);

		// Validate the inputs
		$validator = Validator::make(Input::all(), $rules);

		// Check if the form validates with success
		if ($validator->passes())
		{
			// Save the comment
			$comment = new Comment;
			$comment->user_id = Auth::user()->id;
			$comment->content = Input::get('comment');

			// Was the comment saved with success?
			if($post->comments()->save($comment))
			{
				// Redirect to this blog post page
				return Redirect::to($slug . '#comments')->with('success', 'Your comment was added with success.');
			}

			// Redirect to this blog post page
			return Redirect::to($slug . '#comments')->with('error', 'There was a problem adding your comment, please try again.');
		}

		// Redirect to this blog post page
		return Redirect::to($slug)->withInput()->withErrors($validator);
	}
}
