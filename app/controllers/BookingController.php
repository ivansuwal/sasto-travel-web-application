<?php

class BookingController extends BaseController {

    /**
     * Post Model
     * @var Post
     */
    /**
     * User Model
     * @var User
     */
    protected $user;
    protected $activities;
    protected $activitiesplaces;
    protected $activitiesplacespackages;

    protected $booking_activities;
    protected $booking_rentacar;
    protected $booking_person;
    
    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(BookingPerson $booking_person,User $user,BookingRentaCar $booking_rentacar,Activities $activities,Activitiesplaces $activitiesplaces,Activitiesplacespackages $activitiesplacespackages,Bookingactivities $booking_activities )
    {
        parent::__construct();

        $this->user = $user;
        $this->activities = $activities;
		$this->booking_activities = $booking_activities;
        $this->activitiesplaces = $activitiesplaces;
        $this->activitiesplacespackages = $activitiesplacespackages;
        $this->booking_rentacar = $booking_rentacar;
        $this->booking_person = $booking_person;
        
    }
    
	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getActivities($bookingslug,$package)
	{
		$book = $this->activitiesplaces->where('slug', '=', $bookingslug)->first();
		$packages=$this->activitiesplacespackages->where('id','=',$package)->first();
		// Check if the blog post exists
		if (is_null($book))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		// Show the page
		return View::make('site/booking/book_activities', compact('book','packages'));
	}
	
	public function postActivities($bookingslug,$package)
	{
		$book = $this->activitiesplaces->where('slug', '=', $bookingslug)->first();
		$packages=$this->activitiesplacespackages->where('id','=',$package)->first();
        
		// Check if the blog post exists
		if (is_null($book))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		$rules = array(
            'f_name'   => 'required',
            'l_name'   => 'required',
            'location'   => 'required',
            'email'   => 'required',
            'phone'   => 'required',
            'date'   => 'required',
            'no_of_people'   => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new blog post
            $user = Auth::user();

            // Update the blog post data
            $this->booking_activities->f_name             = Input::get('f_name');
            $this->booking_activities->l_name          = Input::get('l_name');
            $this->booking_activities->location             = Input::get('location');
            $this->booking_activities->phone         = Input::get('phone');
            $this->booking_activities->email            = Input::get('email');
            $this->booking_activities->no_of_people          = Input::get('no_of_people');
            $this->booking_activities->date      = Input::get('date');
            $this->booking_activities->user_id          = $user->id;
            $this->booking_activities->activitiesplaces_id          = $book->id;
            $this->booking_activities->activitiesplacespackages_id          = $packages->id;
            $this->booking_activities->total_price          = $book->price*Input::get('no_of_people');
            
            
            // Was the blog post created?
            if($this->booking_activities->save())
            {
                // Redirect to the new blog post page
                return Redirect::to('/');
            }

            // Redirect to the blog post create page
            return Redirect::to('book/activities/'.$bookingslug)->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('book/activities/'.$bookingslug)->withInput()->withErrors($validator);
    
	}


    public function postRentacar()
    {
        $rules = array(
            'location'   => 'required',
            'typeofcar_id'   => 'required',
            'd_date'   => 'required',
            'a_date'   => 'required',
            'f_name'   => 'required',
            'address'   => 'required',
            'a_phone'   => 'required',
            'phone'   => 'required',
            'email'   => 'required'
            
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new blog post
            $user = Auth::user();

            // Update the blog post data
            $this->booking_rentacar->location             = Input::get('location');
            $this->booking_rentacar->typeofcar_id         = Input::get('typeofcar_id');
            $this->booking_rentacar->prefered_car         = Input::get('prefered_car');
            $this->booking_rentacar->d_date               = Input::get('d_date');
            $this->booking_rentacar->a_date               = Input::get('a_date');
            $this->booking_rentacar->user_id              = $user->id;
            
            $this->booking_person->f_name               = Input::get('f_name');
            $this->booking_person->address              = Input::get('address');
            $this->booking_person->a_phone              = Input::get('a_phone');
            $this->booking_person->phone                = Input::get('phone');
            $this->booking_person->email                = Input::get('email');
            
            
            if($this->booking_rentacar->save())
            {
                $this->booking_person->save();
                // Redirect to the Personal Information form page
                //$from="rentacar";
                //$id=$this->booking_rentacar->id;
                $this->booking_rentacar->bookingperson_id=$this->booking_person->id;
                $this->booking_rentacar->save();
                return Redirect::to('/rentacar')->with('success', "thank you.");
                

                //return Redirect::to('book/rentacar/'.$this->booking_rentacar->id);
            }

            // Redirect to the blog post create page
            return Redirect::to('/rentacar')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('rentacar')->withInput()->withErrors($validator);
    
    }



    
    public function postPersonalinfo($from,$id)
    {
        // Show the page
        if($from=="rentacar")
        {
            $booking_rentacar=$this->booking_rentacar->where('id', '=', $id)->first();
            if (is_null($booking_rentacar))
            {
                return App::abort(404);
            }
        
        }
        // Check if the blog post exists
        $rules = array(
            'f_name'   => 'required',
            'address'   => 'required',
            'a_phone'   => 'required',
            'phone'   => 'required',
            'email'   => 'required',
            
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new blog post
            $user = Auth::user();

            // Update the personal Information of the booker
            $this->booking_person->f_name             = Input::get('f_name');
            $this->booking_person->address          = Input::get('address');
            $this->booking_person->a_phone             = Input::get('a_phone');
            $this->booking_person->phone         = Input::get('phone');
            $this->booking_person->email            = Input::get('email');
            
            
            // Was the blog post created?
            if($this->booking_person->save())
            {
                // Redirect to the new blog post page
                $booking_rentacar->bookingperson_id=$this->booking_person->id;
                $booking_rentacar->save();
                return Redirect::to('/');
            }

            // Redirect to the blog post create page
            return "error";
            //return Redirect::to('book/activities/'.$bookingslug)->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return "valid fail";
        //return Redirect::to('book/activities/'.$bookingslug)->withInput()->withErrors($validator);
    
    }
    
		
}
