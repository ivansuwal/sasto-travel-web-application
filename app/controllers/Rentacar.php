<?php

class ActivitiesController extends BaseController {

    /**
     * Post Model
     * @var Post
     */
    /**
     * User Model
     * @var User
     */
    protected $user;
    **
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(User $user,Typeofcar $typeofcars)
    {
        parent::__construct();

        $this->user = $user;
        $this->typeofcars = $typeofcars;
		
    }
    
	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getView($activity)
	{
		$activities = $this->activities->where('slug', '=', $activity)->first();
		$activitiesplaces = $this->activitiesplaces->where('activities_id', '=', $activities->id)->get();
		
		$unselected_activities=$this->activities->where('name', '<>', $activity)->get();
		// Check if the blog post exists
		if (is_null($activities))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		// Show the page
		return View::make('site/activities/view', compact('activities','activitiesplaces','unselected_activities'));
	}

	public function getViewDetail($activity,$activitydetail)
	{
		$activities = $this->activities->where('name', '=' ,'rafting')->first();
		
		// Check if the blog post exists
		if (is_null($activities))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return App::abort(404);
		}
		// Show the page
		return Datatables::of($activities)
		->make();
		//return View::make('site/activities/view_detail', compact('activities'));
	}


	

	public function getData()
    {
        $activities = Activities::select(array('activities.id', 'activities.name','activities.description'));

        return Datatables::of($activities)        
        ->make();
    }	
}
