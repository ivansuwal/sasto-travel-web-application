<?php

class AdminBookingactivitiesController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $booking_activities;
    protected $activitiesplaces;


    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Bookingactivities $booking_activities,Activitiesplaces $activitiesplaces)
    {
        parent::__construct();
        $this->booking_activities = $booking_activities;
        $this->activitiesplaces=$activitiesplaces;
    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Activities Bookings";

        // Grab all the hotel posts
        $booking_activities = $this->booking_activities->get();

        // Show the page
        return View::make('admin/booking/activities/index', compact('booking_activites', 'title'));
    }

    public function getShow($id)
    {
        $booking_activities=$this->booking_activities->where('id','=',$id)->first();
        $title="Booking Details";
        $activitiesplaces=$this->activitiesplaces->where('id','=',$booking_activities->activitiesplaces_id)->first();
        return View::make('admin/booking/activities/show', compact('booking_activities','activitiesplaces', 'title'));

    }

    public function getEdit($id)
    {
        // Title
        $booking_activities=$this->booking_activities->where('id','=',$id)->first();
        $title = "Edit Boooking | Activities";
        $activitiesplaces=$this->activitiesplaces->get();


        // Show the page
    return View::make('admin/booking/activities/edit', compact('booking_activities','activitiesplaces', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($id)
    {
        $booking_activities=$this->booking_activities->where('id','=',$id)->first();

        // Declare the rules for the form validation
        $rules = array(
            'f_name'   => 'required|min:3',
            'l_name' => 'required|min:1',
            'email' => 'required',
            'location' => 'required',
            'phone' => 'required',
            'activitiesplaces_id' => 'required',
            'date' => 'required',
            'no_of_people' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $booking_activities->f_name            = Input::get('f_name');
            $booking_activities->l_name             = Input::get('l_name');
            $booking_activities->email             = Input::get('email');
            $booking_activities->location             = Input::get('location');
            $booking_activities->phone             = Input::get('phone');
            $booking_activities->activitiesplaces_id             = Input::get('activitiesplaces_id');
            $booking_activities->date             = Input::get('date');
            $booking_activities->no_of_people             = Input::get('no_of_people');
            
            // Was the activity updated?
            if($booking_activities->save())
            {
                // Redirect to the new blog post page
              return Redirect::to('admin/booking/activities/' . $booking_activities->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the booking_activities post management page
            return Redirect::to('admin/booking/activities/' . $booking_activities->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/booking/activities/' . $booking_activities . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($id)
    {
        // Title
        $title = "Delete Booking | Activities";

        $booking_activities=$this->booking_activities->where('id','=',$id)->first();

        // Show the page
        return View::make('admin/booking/activities/delete', compact('booking_activities', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($id)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        $booking_activities=$this->booking_activities->where('id','=',$id)->first();

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $booking_activities->id;
            $booking_activities->delete();
            
            // Was the activitie activities deleted?
            $booking_activities = Bookingactivities::find($id);
            if(empty($booking_activities))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/booking/activities')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/booking/activities')->with('error', "Error! while deleting.");
    }
    
public function getStatus($id)
    {
        // Title
        $title = "Change Status | Activities";

        $booking_activities=$this->booking_activities->where('id','=',$id)->first();

        // Show the page
        return View::make('admin/booking/activities/status', compact('booking_activities', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postStatus($id)
    {
        // Declare the rules for the form validation
        $rules = array(
            'status' => 'required|integer'
        );

        $booking_activities=$this->booking_activities->where('id','=',$id)->first();

        $booking_activities->status=Input::get('status');
        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $booking_activities->save();
            
            // Was the activitie activities deleted?
                return Redirect::to('admin/booking/activities')->with('success', "Status Changed Succesful");
            
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/booking/activities')->with('error', "Error! while changing status.");
    }
    

    
	public function getData()
    {
        $booking_activites = Bookingactivities::select(array('booking_activities.id','f_name','location','phone','activitiesplaces_id','date','no_of_people'));

        return Datatables::of($booking_activites)
        
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/booking/activities/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/booking/activities/\' . $id . \'/status\' ) }}}" class="btn btn-default btn-xs iframe" >Status</a>
            <a href="{{{ URL::to(\'admin/booking/activities/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/booking/activities/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        ->make();
    }

}