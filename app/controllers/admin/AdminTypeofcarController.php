<?php

class AdminTypeofcarController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $typeofcar;
    

    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Typeofcar $typeofcar)
    {
        parent::__construct();
        $this->typeofcar = $typeofcar;

    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Type of Cars Management";

        // Grab all the hotel posts
        $typeofcar = $this->typeofcar->get();

        // Show the page
        return View::make('admin/typeofcar/index', compact('typeofcar', 'title'));
    }


    public function getCreate()
    {
        // Title
        $title = "Add a New Type of Car for Rental";

        // Show the page
        return View::make('admin/typeofcar/create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'price' => 'required',
            'seats' => 'required'

        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new activitie post
            $user = Auth::user();

            // Update the blog post data
            $this->typeofcar->name            = Input::get('name');
            $this->typeofcar->price             = Input::get('price');
            $this->typeofcar->seats         = Input::get('seats');
            $this->typeofcar->user_id     =$user->id;
            // Was the blog post created?
            if($this->typeofcar->save())
            {
                // Redirect to the new blog post page
                return Redirect::to('admin/typeofcar/' . $this->typeofcar->id . '/edit')->with('success',"New Type of Car Added");
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/typeofcar/create')->with('error', "Error! Please Check.");
        }

        // Form validation failed
        return Redirect::to('admin/typeofcar/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getShow($typeofcar)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($typeofcar)
    {
        // Title
        $title = "Edit Type Of Car";

        // Show the page
        return View::make('admin/typeofcar/create_edit', compact('typeofcar', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($typeofcar)
    {
        
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'price' => 'required',
            'seats' => 'required'

        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $typeofcar->name            = Input::get('name');
            $typeofcar->price             = Input::get('price');
            $typeofcar->seats         = Input::get('seats');
            // Was the activity updated?
            if($typeofcar->save())
            {
                // Redirect to the new blog post page
              return Redirect::to('admin/typeofcar/' . $typeofcar->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the typeofcar post management page
            return Redirect::to('admin/typeofcar/' . $typeofcar->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/typeofcar/' . $typeofcar->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($typeofcar)
    {
        // Title
        $title = "Delete a Type of car";

        // Show the page
        return View::make('admin/typeofcar/delete', compact('typeofcar', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($typeofcar)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $typeofcar->id;
            $typeofcar->delete();

            // Was the activitie typeofcar deleted?
            $typeofcar = Typeofcar::find($id);
            if(empty($typeofcar))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/typeofcar')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/typeofcar')->with('error', "Error! while deleting.");
    }
    
	public function getData()
    {
        $typeofcar = Typeofcar::select(array('type_of_car.id', 'type_of_car.name','type_of_car.price', 'type_of_car.seats'));

        return Datatables::of($typeofcar)
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/typeofcar/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/typeofcar/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        
        ->make();
    }

}