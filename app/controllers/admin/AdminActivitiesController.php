<?php

class AdminActivitiesController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $activities;
    protected $activitiesplaces;


    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Activities $activities, Activitiesplaces $activitiesplaces)
    {
        parent::__construct();
        $this->activities = $activities;
        $this->activitiesplaces = $activitiesplaces;

    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Activities Management";

        // Grab all the hotel posts
        $activities = $this->activities;

        // Show the page
        return View::make('admin/activities/index', compact('activities', 'title'));
    }


    public function getCreate()
    {
        // Title
        $title = "Create a New Activity";

        // Show the page
        return View::make('admin/activities/create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'description' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new activitie post
            $user = Auth::user();

            // Update the blog post data
            $this->activities->name            = Input::get('name');
            $this->activities->slug             = Str::slug(Input::get('name'));
            $this->activities->description         = Input::get('description');
            $destinationPath = '';
            $filename        = '';

            if (Input::hasFile('image')) {
                $file            = Input::file('image');
                $destinationPath = 'img/activities/';
                $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                
                $uploadSuccess   = $file->move($destinationPath, $filename);
            }
            $this->activities->image_path         = $destinationPath . $filename;
            
            // Was the blog post created?
            if($this->activities->save())
            {
                // Redirect to the new blog post page
                return Redirect::to('admin/activities/' . $this->activities->id . '/edit')->with('success',"New Activity Added");
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/activities/create')->with('error', "Error! Please Check.");
        }

        // Form validation failed
        return Redirect::to('admin/activities/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getShow($activities)
    {
        $title = "Activity Detail";
        // Show the page
        return View::make('admin/activities/show', compact('activities', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($activities)
    {
        // Title
        $title = "Edit Activity";

        // Show the page
        return View::make('admin/activities/edit', compact('activities', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($activities)
    {

        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'description' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $activities->name            = Input::get('name');
            $activities->slug             = Str::slug(Input::get('name'));
            $activities->description         = Input::get('description');
            
            // Was the activity updated?
            if($activities->save())
            {
                $destinationPath = '';
                $filename        = '';
                //Was a new image added?(i.e old image deleted and new image added)
                if (Input::hasFile('image')) 
                {
                    $file            = Input::file('image');
                    $destinationPath = 'img/activities/';
                    $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                    
                    $uploadSuccess   = $file->move($destinationPath, $filename);
                    $activities->image_path         = $destinationPath . $filename;
                    $activities->save();
                }

                // Redirect to the new blog post page
              return Redirect::to('admin/activities/' . $activities->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the activities post management page
            return Redirect::to('admin/activities/' . $activities->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/activities/' . $activities->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($activities)
    {
        // Title
        $title = "Delete Activity";

        // Show the page
        return View::make('admin/activities/delete', compact('activities', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($activities)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $activities->id;
            $activities->delete();

            // Was the activitie activities deleted?
            $activities = Activities::find($id);
            if(empty($activities))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/activities')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/activities')->with('error', "Error! while deleting.");
    }
    
	public function getData()
    {
        $activities = Activities::select(array('activities.id', 'activities.name','activities.image_path as image'));

        return Datatables::of($activities)
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/activities/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/activities/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/activities/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        ->edit_column('image', '<a target="_blank" href="{{{URL::to("$image")}}}">View Image</a>')

               
        ->make();
    }

}