<?php

class AdminAmenitiesController extends AdminController {


    protected $amenities;
   

    public function __construct(Amenities $amenities)
    {
        parent::__construct();
        $this->amenities = $amenities;
    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Amenities Management";

        // Grab all the hotel posts
        $amenities = $this->amenities;

        // Show the page
        return View::make('admin/amenities/index', compact('amenities', 'title'));
    }


    public function getCreate()
    {
        // Title
        $title = "Add a new Amenities";

        // Show the page
        return View::make('admin/amenities/create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:1',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new activitie post
            $user = Auth::user();

            // Update the amenities data
            $this->amenities->name            = Input::get('name');
            
            // Was the amenity created?
            if($this->amenities->save())
            {
                // Redirect to the new blog post page
                return Redirect::to('admin/amenities/' . $this->amenities->id . '/edit')->with('success',"New Activity Added");
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/amenities/create')->with('error', "Error! Please Check.");
        }

        // Form validation failed
        return Redirect::to('admin/amenities/create')->withInput()->withErrors($validator);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($amenities)
    {
        // Title
        $title = "Edit Activity";

        // Show the page
        return View::make('admin/amenities/create_edit', compact('amenities', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($amenities)
    {

        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:1',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $amenities->name            = Input::get('name');
            
            // Was the activity updated?
            if($amenities->save())
            {
                // Redirect to the new blog post page
              return Redirect::to('admin/amenities/' . $amenities->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the amenities post management page
            return Redirect::to('admin/amenities/' . $amenities->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/amenities/' . $amenities->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($amenities)
    {
        // Title
        $title = "Delete the Amenity";

        // Show the page
        return View::make('admin/amenities/delete', compact('amenities', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($amenities)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $amenities->id;
            $amenities->delete();

            // Was the activitie amenities deleted?
            $amenities = amenities::find($id);
            if(empty($amenities))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/amenities')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/amenities')->with('error', "Error! while deleting.");
    }
    
	public function getData()
    {
        $amenities = Amenities::select(array('amenities.id', 'amenities.name'));

        return Datatables::of($amenities)
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/amenities/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/amenities/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
               
        ->make();
    }

}