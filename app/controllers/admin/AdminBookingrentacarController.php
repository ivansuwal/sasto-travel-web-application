<?php

class AdminBookingrentacarController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $typeofcar;
    protected $booking_rentacar;
    protected $booking_person;
    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Bookingperson $booking_person,Bookingrentacar $booking_rentacar,Typeofcar $typeofcar)
    {
        parent::__construct();
        $this->typeofcar = $typeofcar;
        $this->booking_rentacar=$booking_rentacar;
        $this->booking_person=$booking_person;
        
    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Rental Cars Bookings";

        // Grab all the hotel posts
        $booking_rentacar = $this->booking_rentacar->get();

        // Show the page
        return View::make('admin/booking/rentacar/index', compact('booking_rentacar', 'title'));
    }

    public function getShow($booking_rentacar)
    {
        $title="Booking Details";
        $booking_person=$this->booking_person->where('id','=',$booking_rentacar->bookingperson_id)->first();
        $typeofcar=$this->typeofcar->where('id','=',$booking_rentacar->typeofcar_id)->first();
        return View::make('admin/booking/rentacar/show', compact('booking_rentacar','typeofcar','booking_person', 'title'));

    }


    public function getEdit($booking_rentacar)
    {
        // Title
        $title = "Edit Boooking | Activities";
        
        $typeofcar=Typeofcar::all();
        // Show the page
        $booking_person=$this->booking_person->where('id','=',$booking_rentacar->bookingperson_id)->first();
        
        return View::make('admin/booking/rentacar/edit', compact('id','booking_rentacar','typeofcar','booking_person', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($booking_rentacar)
    {
        $booking_person=$this->booking_person->where('id','=',$booking_rentacar->bookingperson_id)->first();
        // Declare the rules for the form validation
        $rules = array(
            'f_name'   => 'required|min:3',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'location' => 'required',
            'typeofcar_id' => 'required',
            'd_date' => 'required',
            'a_date' => 'required'
            );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $booking_person->f_name            = Input::get('f_name');
            $booking_person->address             = Input::get('address');
            $booking_person->email             = Input::get('email');
            $booking_person->phone             = Input::get('phone');
            $booking_rentacar->location             = Input::get('location');
            $booking_rentacar->prefered_car             = Input::get('prefered_car');
            $booking_rentacar->typeofcar_id             = Input::get('typeofcar_id');
            $booking_rentacar->d_date             = Input::get('d_date');
            $booking_rentacar->a_date             = Input::get('a_date');
            
            // Was the activity updated?
            if($booking_rentacar->save())
            {
                $booking_person->save();
                // Redirect to the new blog post page
              return Redirect::to('admin/booking/rentacar/' . $booking_rentacar->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the booking_rentacar post management page
            return Redirect::to('admin/booking/rentacar/' . $booking_rentacar->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/booking/rentacar/' . $booking_rentacar->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($booking_rentacar)
    {
        // Title
        $title = "Delete Booking | Rent a Car";
        // Show the page
        return View::make('admin/booking/rentacar/delete', compact('booking_rentacar', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($booking_rentacar)
    {
        
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $booking_rentacar->id;
            $booking_rentacar->delete();
            
            // Was the activitie activities deleted?
            $booking_rentacar = Bookingrentacar::find($id);
            if(empty($booking_rentacar))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/booking/rentacar')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/booking/rentacar')->with('error', "Error! while deleting.");
    }
    
    
	public function getData()
    {
        $booking_rentacar = Bookingrentacar::select(array('id','bookingperson_id as person','location','typeofcar_id','d_date','a_date'));

        return Datatables::of($booking_rentacar)
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/booking/rentacar/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/booking/rentacar/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/booking/rentacar/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        
        ->make();
    }

}