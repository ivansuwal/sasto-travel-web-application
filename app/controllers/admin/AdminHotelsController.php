<?php

class AdminHotelsController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $hotels;
    protected $hotelsimages;
    protected $hotelsfacilities;
    protected $roomfacilities;
    protected $roomtypes;
    protected $amenities;


    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Hotels $hotels,Amenities $amenities,Hotelsimages $hotelsimages,Hotelsfacilities $hotelsfacilities,Roomfacilities $roomfacilities,Roomtypes $roomtypes)
    {
        parent::__construct();
        $this->hotels = $hotels;
        $this->hotelsimages = $hotelsimages;
        $this->hotelsfacilities = $hotelsfacilities;
        $this->roomfacilities = $roomfacilities;
        $this->roomtypes = $roomtypes;
        $this->amenities = $amenities;

    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/hotels/title.hotel_management');

        // Grab all the hotel posts
        $hotels = $this->hotels;

        // Show the page
        return View::make('admin/hotels/index', compact('hotels', 'title'));
    }

    /**
     * View a hotel post.
     *
     * @param  string  $slug
     * @return View
     * @throws NotFoundHttpException
     */
    public function getView($slug)
    {
        // Get this hotel post data
        $hotels = $this->hotels->where('id', '=', $slug)->first();

        // Check if the hotel post exists
        if (is_null($hotels))
        {
            // If we ended up in here, it means that
            // a page or a hotel post didn't exist.
            // So, this means that it is time for
            // 404 error page.
            return App::abort(404);
        }

        /*// Get this post comments
        $comments = $post->comments()->orderBy('created_at', 'ASC')->get();

        // Get current user and check permission
        $user = $this->user->currentUser();
        $canComment = false;
        if(!empty($user)) {
            $canComment = $user->can('post_comment');
        }
*/
        // Show the page
        return View::make('admin/hotels/view_hotel', compact('hotels'));//, 'comments', 'canComment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // Title
        $title = Lang::get('admin/hotels/title.create_a_new_blog');
        $amenities=$this->amenities->get();
        // Show the page
        return View::make('admin/hotels/create_edit', compact('amenities','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'city' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new blog post
            $user = Auth::user();

            // Update the blog post data
            $this->hotels->name            = Input::get('name');
            $this->hotels->city             = Input::get('city');
            $this->hotels->phone          = Input::get('phone');
            
            // Was the hotel created?
            if($this->hotels->save())
            {
                //store images
                //store images 
                for ($i=0; $i < count(Input::file('image')); $i++) 
                { 
                    $destinationPath = '';
                    $filename        = '';

                    if (Input::hasFile('image.'.$i)) 
                    {
                        $file            = Input::file('image.'.$i);
                        $destinationPath = 'img/hotels/';
                        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                        
                        $uploadSuccess   = $file->move($destinationPath, $filename);
                        $image = new Hotelsimages;
                        $image->image_path         = $destinationPath . $filename;
                        $image->hotels_id = $this->hotels->id;
                        $image->save();
                    }
                }
                
                //Store room types and details
                for ($i=0; $i < count(Input::get('room_name')); $i++) 
                { 
                    if (Input::has('room_name.'.$i)) 
                    {
                            $roomtypes=new Roomtypes;
                            $roomtypes->hotel_id = $this->hotels->id;
                            $roomtypes->name=Input::get('room_name.'.$i);
                            $roomtypes->max_person=Input::get('room_max_person.'.$i);
                            $roomtypes->price=Input::get('room_price.'.$i);
                            $roomtypes->no_of_rooms=Input::get('no_of_rooms.'.$i);
                            $roomtypes->available_rooms=Input::get('available_rooms.'.$i);
                           if($roomtypes->save())
                           {
                                for ($j=0; $j < count(Input::get('room_amenities_'.($i+1))); $j++) 
                                {   
                                    if(Input::has('room_amenities_'.($i+1).'.'.$j))
                                    {
                                        $roomfacilities=new Roomfacilities;
                                        $roomfacilities->room_type_id=$roomtypes->id;
                                        $roomfacilities->amenities_id=Input::get('room_amenities_'.($i+1).'.'.$j);
                                        $roomfacilities->save();
                                    }
                                }
                           }
                            
                    }      
                }
                

                //Hotel Id in hotel facilities
                for ($i=0; $i < count(Input::get('hotel_amenities')); $i++) 
                {   if(Input::has('hotel_amenities.'.$i))
                    {
                        $hotelsfacilities=new Hotelsfacilities;
                        $hotelsfacilities->hotel_id=$this->hotels->id;
                        $hotelsfacilities->amenities_id=Input::get('hotel_amenities.'.$i);
                        $hotelsfacilities->save();
                    }
                }
                
                // Redirect to the newly added hotel edit page
                return Redirect::to('admin/hotels/' . $this->hotels->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            // Redirect to the hotel create page
            return Redirect::to('admin/hotels/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/hotels/create')->withInput()->withErrors($validator);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($hotels)
    {
        // Title
        $title = Lang::get('admin/hotels/title.hotel_update');
        $amenities=$this->amenities->get();
        $hotelsimages=$this->hotelsimages->where('hotels_id','=',$hotels->id)->get();
        $hotelsfacilities=$this->hotelsfacilities->where('hotel_id','=',$hotels->id)->get();
        $roomtypes=$this->roomtypes->where('hotel_id','=',$hotels->id)->get();
        $roomfacilities=$this->roomfacilities->select('room_type_id','amenities_id')->join('room_type','room_type.id','=','room_facilities.room_type_id')->where('hotel_id','=',$hotels->id)->get();
        // Show the page
        return View::make('admin/hotels/edit', compact('hotels','roomfacilities','hotelsimages','roomtypes','hotelsfacilities','amenities', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($hotels)
    {

        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'city' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the hotel data
            $hotels->name            = Input::get('name');
            $hotels->city            =Input::get('city');
            $hotels->phone          = Input::get('phone');
            
            // Was the hotel post updated?
            if($hotels->save())
            {
                //get all images of the hotel
                $hotelsimages=$this->hotelsimages->where('hotels_id','=',$hotels->id)->get();
                //Check if any previous image in the database 
                foreach ($hotelsimages as $image) 
                {
                    //Suppose,image is not present in the submitted form
                    $exists="false";
                    for ($i=0; $i <count(Input::get('eimage_id')) ; $i++) 
                    { 
                        //Is the image present  in the form?
                        if($image->id==Input::get('eimage_id.'.$i))
                        {
                            //If,yes then true
                            $exists="true";
                        }
                    }

                    if($exists=="false")
                    {
                        //delete the image if $exists=false
                        $image->delete();
                    }
                }

                //check for any new image added and upload,add to database
                for ($i=0; $i < count(Input::file('image')); $i++) 
                { 
                    $destinationPath = '';
                    $filename        = '';

                    if (Input::hasFile('image.'.$i)) 
                    {
                        $file            = Input::file('image.'.$i);
                        $destinationPath = 'img/hotels/';
                        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                        
                        $uploadSuccess   = $file->move($destinationPath, $filename);
                        $image = new Hotelsimages;
                        $image->image_path         = $destinationPath . $filename;
                        $image->hotels_id = $hotels->id;
                        $image->save();
                    }
                }

                //get all hotel facilities of the hotel
                $hotelsfacilities=$this->hotelsfacilities->where('hotel_id','=',$hotels->id)->get();
                //for all checked hotel_facilities in the edit form
                for ($i=0; $i < count(Input::get('hotel_amenities')); $i++) 
                {
                    if(Input::has('hotel_amenities.'.$i))
                    {
                        $exists="false";
                        foreach($hotelsfacilities as $hf)
                        {   
                            //Is the checked facility is in database already?
                            if(Input::get('hotel_amenities.'.$i)==$hf->amenities_id)
                            {
                                //return Input::get('hotel_amenities.'.$i);
                                $exists="true";
                            }
                        }
                        //If the checked facility is not in the database already, then insert it.
                        if($exists=="false")
                        {
                            $hotelsfacility=new Hotelsfacilities;
                            $hotelsfacility->hotel_id=$hotels->id;
                            $hotelsfacility->amenities_id=Input::get('hotel_amenities.'.$i);
                            $hotelsfacility->save();
                        }
                    }
                }
                //get all hotel facilities of the hotel
                $hotelsfacilities=$this->hotelsfacilities->where('hotel_id','=',$hotels->id)->get();
                foreach($hotelsfacilities as $hf)
                {   
                    $exists="false";
                    for ($i=0; $i < count(Input::get('hotel_amenities')); $i++) 
                    {
                        //Was the facility is in database and checked in the edit form?
                        if(Input::get('hotel_amenities.'.$i)==$hf->amenities_id)
                        {
                            $exists="true";
                        }
                    }
                    //If the unchecked facilities are in database then delete from database
                    if($exists=="false")
                    {
                        $hf->delete();
                    }
                }
                
                //get all room types of the hotel
                $roomtypes=$this->roomtypes->where('hotel_id','=',$hotels->id)->get();
                //Check all previous room types in the database 
                foreach ($roomtypes as $rt) 
                {
                    //Suppose,room type is not present in the submitted form
                    $exists="false";
                    for ($i=0; $i <count(Input::get('eroom_id')); $i++) 
                    { 
                        //Is the room type present  in the form?
                        if($rt->id==Input::get('eroom_id.'.$i))
                        {
                            //If,yes then true
                            $exists="true";
                            //update the rt data
                            $rt->name=Input::get('eroom_name.'.$i);
                            $rt->max_person=Input::get('eroom_max_person.'.$i);
                            $rt->price=Input::get('eroom_price.'.$i);
                            $rt->no_of_rooms=Input::get('eno_of_rooms.'.$i);
                            $rt->available_rooms=Input::get('eavailable_rooms.'.$i);
                            if($rt->save())
                            {
                                  
                                    //get all room facilities of the room type
                                    $roomfacilities=$this->roomfacilities->where('room_type_id','=',$rt->id)->get();
                                    //for all checked room_facilities in the edit form
                                    for ($j=0; $j < count(Input::get('eroom_amenities_'.($i+1))); $j++) 
                                    {
                                        if(Input::has('eroom_amenities_'.($i+1).'.'.$j))
                                        {
                                            $exists="false";
                                            foreach($roomfacilities as $rf)
                                            {   
                                                //Is the checked facility is in database already?
                                                if((Input::get('eroom_amenities_'.($i+1).'.'.$j))==$rf->amenities_id)
                                                {
                                                    //return Input::get('hotel_amenities.'.$i);
                                                    $exists="true";
                                                }
                                            }
                                            //If the checked facility is not in the database already, then insert it.
                                            if($exists=="false")
                                            {
                                                $roomfacility=new Roomfacilities;
                                                $roomfacility->room_type_id=$rt->id;
                                                $roomfacility->amenities_id=Input::get('eroom_amenities_'.($i+1).'.'.$j);
                                                $roomfacility->save();
                                            }
                                        }
                                    }
                                    //get all hotel facilities of the hotel
                                    $roomfacilities=$this->roomfacilities->where('room_type_id','=',$rt->id)->get();
                                    foreach($roomfacilities as $rf)
                                    {   
                                        $exists="false";
                                        for ($j=0; $j < count(Input::get('eroom_amenities_'.($i+1))); $j++) 
                                        {
                                            //Was the facility is in database and checked in the edit form?
                                            if(Input::get('eroom_amenities_'.($i+1).'.'.$j)==$rf->amenities_id)
                                            {
                                                $exists="true";
                                            }
                                        }
                                        //If the unchecked facilities are in database then delete from database
                                        if($exists=="false")
                                        {
                                            $rf->delete();
                                        }
                                        $exists="true";
                                    }
                            }
                            
                        }
                    }
                    if($exists=="false")
                    {
                        //delete the rt if not present in the submitted form
                        $rt->delete();
                        //return $exists;
                        
                    }
                }


                //Store newly added room types and details
                for ($i=0; $i < count(Input::get('room_name')); $i++) 
                { 
                    if (Input::has('room_name.'.$i)) 
                    {
                            $roomtypes=new Roomtypes;
                            $roomtypes->hotel_id = $hotels->id;
                            $roomtypes->name=Input::get('room_name.'.$i);
                            $roomtypes->max_person=Input::get('room_max_person.'.$i);
                            $roomtypes->price=Input::get('room_price.'.$i);
                            $roomtypes->no_of_rooms=Input::get('no_of_rooms.'.$i);
                            $roomtypes->available_rooms=Input::get('available_rooms.'.$i);
                           if($roomtypes->save())
                           {
                                for ($j=0; $j < count(Input::get('room_amenities_'.($i+1))); $j++) 
                                {   
                                    if(Input::has('room_amenities_'.($i+1).'.'.$j))
                                    {
                                        $roomfacilities=new Roomfacilities;
                                        $roomfacilities->room_type_id=$roomtypes->id;
                                        $roomfacilities->amenities_id=Input::get('room_amenities_'.($i+1).'.'.$j);
                                        $roomfacilities->save();
                                    }
                                }
                           }
                            
                    }      
                }
                

                // Redirect to the new hotel post page
                return Redirect::to('admin/hotels/' . $hotels->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            // Redirect to the blogs post management page
            return Redirect::to('admin/hotels/' . $hotels->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        // Form validation failed
        return Redirect::to('admin/hotels/' . $hotels->id . '/edit')->withInput()->withErrors($validator);
    }


    public function getShow($hotels)
    {
        $title="Hotel Details";
        $hotelsimages=$this->hotelsimages->where('hotels_id','=',$hotels->id)->get();
        $hotelsfacilities=$this->hotelsfacilities->where('hotel_id','=',$hotels->id)->join('amenities','amenities.id','=','hotels_facilities.amenities_id')->get();
        $roomtypes=$this->roomtypes->where('hotel_id','=',$hotels->id)->get();
        foreach ($roomtypes as $type) {
            $roomfacilities[]=$this->roomfacilities->select('room_type_id','amenities.name')->join('amenities','amenities.id','=','room_facilities.amenities_id')->where('room_type_id','=',$type->id)->get();
        }
        // Show the page
        return View::make('admin/hotels/show', compact('hotels','hotelsfacilities','hotelsimages','roomfacilities','roomtypes','title'));
    
    }


	public function getData()
    {
        $hotels = Hotels::select(array('hotels.id', 'hotels.name', 'hotels.city', 'hotels.phone'));

        return Datatables::of($hotels)
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/hotels/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/hotels/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/hotels/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        /*
        ->edit_column('facilities', function ($hotels) {
                return DB::table('hotels_facilities')->where('hotel_id', '=', $hotels->id )->get();
            })
        ->add_column('rooms', function ($hotel) {
                return DB::table('room_type')->where('hotel_id', '=', $hotels->id )->get();
            })
        */
        ->make();
    }

}