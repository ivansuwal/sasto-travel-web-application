<?php

class AdminPackagesController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $packages;
    protected $itinerary;
    

    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Packages $packages,Itinerary $itinerary)
    {
        parent::__construct();
        $this->packages = $packages;
        $this->itinerary=$itinerary;
    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Packages Management";

        // Grab all the hotel posts
        $packages = $this->packages;

        // Show the page
        return View::make('admin/packages/index', compact('packages', 'title'));
    }


    public function getCreate()
    {
        // Title
        $title = "Create a New Package";

        // Show the page
        return View::make('admin/packages/create_edit', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'title'   => 'required|min:3',
            'price'   => 'required',
            'start_date'   => 'required',
            'end_date'   => 'required',
            'vendor'   => 'required',
            'description' => 'required|min:3'
            
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new activitie post
            $user = Auth::user();

            // Update the blog post data
            $this->packages->title            = Input::get('title');
            $this->packages->slug             = Str::slug(Input::get('title'));
            $this->packages->price            = Input::get('price');
            $this->packages->price_offer            = Input::get('price_offer');
            $this->packages->start_date            = Input::get('start_date');
            $this->packages->end_date            = Input::get('end_date');
            $this->packages->vendor            = Input::get('vendor');
            $this->packages->description         = Input::get('description');
            $this->packages->user_id         = $user->id;
            $destinationPath = '';
            $filename        = '';

            if (Input::hasFile('image')) {
                $file            = Input::file('image');
                $destinationPath = 'img/packages/';
                $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                
                $uploadSuccess   = $file->move($destinationPath, $filename);
            }
            $this->packages->image_path         = $destinationPath . $filename;
            
            // Was the blog post created?
            if($this->packages->save())
            {
                //get inserted packages's primary key-id
                $id=$this->packages->id;

                for ($i=0; $i < count(Input::get('i_day')); $i++) { 
                    $itinerary=new Itinerary;
                    $itinerary->packages_id = $id;
                    $itinerary->day=Input::get('i_day.'.$i);
                    $itinerary->description=Input::get('i_day_description.'.$i);
                    $itinerary->save();
                }
                // Redirect to the new blog post page
                return Redirect::to('admin/packages/' . $this->packages->id . '/edit')->with('success',"New Package Added");
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/packages/create')->with('error', "Error! Please Check.");
        }

        // Form validation failed
        return Redirect::to('admin/packages/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getShow($packages)
    {
        $title="Package Details";
        $itinerary=$this->itinerary->where('packages_id','=',$packages->id)->get();
        
        // Show the page
        return View::make('admin/packages/show', compact('packages','itinerary','title'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($packages)
    {
        // Title
        $title = "Edit Package";
        $itinerary=$this->itinerary->where('packages_id','=',$packages->id)->get();
        
        // Show the page
        return View::make('admin/packages/edit', compact('packages','itinerary', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($packages)
    {

        // Declare the rules for the form validation
        $rules = array(
            'title'   => 'required|min:3',
            'price'   => 'required',
            'start_date'   => 'required',
            'end_date'   => 'required',
            'vendor'   => 'required',
            'description' => 'required|min:3'
            
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $packages->title            = Input::get('title');
            $packages->slug             = Str::slug(Input::get('title'));
            $packages->price            = Input::get('price');
            $packages->price_offer            = Input::get('price_offer');
            $packages->start_date            = Input::get('start_date');
            $packages->end_date            = Input::get('end_date');
            $packages->vendor            = Input::get('vendor');
            $packages->description         = Input::get('description');
            
            // Was the activity updated?
            if($packages->save())
            {
                $destinationPath = '';
                $filename        = '';
                //Was a new image added?(i.e old image deleted and new image added)
                if (Input::hasFile('image')) 
                {
                    $file            = Input::file('image');
                    $destinationPath = 'img/activities/';
                    $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                    
                    $uploadSuccess   = $file->move($destinationPath, $filename);
                    $packages->image_path         = $destinationPath . $filename;
                    $packages->save();
                }

                //get all itineraries of the activityplace
                $itineraries=$this->itinerary->where('packages_id','=',$packages->id)->get();
                $day=1;
                    
                //Check all previous packages in the database 
                foreach ($itineraries as $itinerary) 
                {
                    //Suppose,itinerary is not present in the submitted form
                    $exists="false";
                    for ($i=0; $i <count(Input::get('ei_day_id')) ; $i++) 
                    { 
                        //Is the itinerary present  in the form?
                        if($itinerary->id==Input::get('ei_day_id.'.$i))
                        {
                            //If,yes then true
                            $exists="true";
                            //update the itinerary data
                            $itinerary->day            = $day;
                            $itinerary->description            = Input::get('ei_day_description.'.$i);
                            $itinerary->save();
                            $day = $day + 1;
                            
                        }
                    }

                    if($exists=="false")
                    {
                        //delete the itinerary if $exists=false
                        $itinerary->delete();
                    }
                }

                //check for any itinerary added and if any add to database
                for ($i=0; $i < count(Input::get('i_day')); $i++) { 
                    $itinerary=new Itinerary;
                    $itinerary->packages_id = $packages->id;
                    $itinerary->day=$day;
                    $itinerary->description=Input::get('i_day_description.'.$i);
                    $itinerary->save();
                    $day = $day +1;
                }
                


                // Redirect to the new blog post page
                return Redirect::to('admin/packages/' . $packages->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the packages post management page
            return Redirect::to('admin/packages/' . $packages->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/packages/' . $packages->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($packages)
    {
        // Title
        $title = "Delete a Package";

        // Show the page
        return View::make('admin/packages/delete', compact('packages', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($packages)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            
            $id = $packages->id;
            $packages->delete();
            
            // Was the blog packages deleted?
            $packages = Packages::find($id);
            if(empty($packages))
            {
                // Redirect to the blog packagess management page
                return Redirect::to('admin')->with('success',"Packages Deleted Successfully!");
            }
        }
        // There was a problem deleting the blog packages
        return Redirect::to('admin/packages')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }

	public function getData()
    {
        $packages = Packages::select(array('packages.id', 'packages.title', 'packages.price', 'packages.price_offer', 'packages.vendor','packages.start_date', 'packages.end_date'));

        return Datatables::of($packages)
        ->add_column('activityons', 
            '<a href="{{{ URL::to(\'admin/packages/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/packages/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/packages/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
               
        ->make();
    }

}