<?php

class AdminActivitiesplacesController extends AdminController {


    /**
     * Post Model
     * @var Post
     */
    protected $activities;
    protected $activitiesplaces;
    protected $activitiesplacesimages;
    protected $activitiesplacespackages;


    /**
     * Inject the models.
     * @param Post $post
     */
    public function __construct(Activities $activities, Activitiesplaces $activitiesplaces,Activitiesplacesimages $activitiesplacesimages,Activitiesplacespackages $activitiesplacespackages)
    {
        parent::__construct();
        $this->activities = $activities;
        $this->activitiesplaces = $activitiesplaces;
        $this->activitiesplacesimages = $activitiesplacesimages;
        $this->activitiesplacespackages = $activitiesplacespackages;
    }

    /**
     * Show a list of all the hotel posts.
     *
     * @return View
     */
    public function getIndex()
    {
        // Title
        $title = "Activities Places Management";

        // Grab all the hotel posts
        $activitiesplaces = $this->activitiesplaces;

        // Show the page
        return View::make('admin/activitiesplaces/index', compact('activitiesplaces', 'title'));
    }


    public function getCreate()
    {
        // Title
        $title = "Create a New Activity Place";

        $activities=$this->activities->get();
        // Show the page
        return View::make('admin/activitiesplaces/create_edit', compact('activities','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $activities=$this->activities;
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'description' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Create a new activitieplace post
            $user = Auth::user();

            // Update the blog post data
            $this->activitiesplaces->name            = Input::get('name');
            $this->activitiesplaces->slug             = Str::slug(Input::get('name'));
            $this->activitiesplaces->description         = Input::get('description');
            $this->activitiesplaces->activities_id         = Input::get('activities_id');
            $this->activitiesplaces->youtube_link         = Input::get('price');
            
            // Was the activity place created?
            if($this->activitiesplaces->save())
            {
                //store images 
                for ($i=0; $i < count(Input::file('image')); $i++) 
                { 
                    $destinationPath = '';
                    $filename        = '';

                    if (Input::hasFile('image.'.$i)) 
                    {
                        $file            = Input::file('image.'.$i);
                        $destinationPath = 'img/activities/';
                        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                        
                        $uploadSuccess   = $file->move($destinationPath, $filename);
                        $image = new Activitiesplacesimages;
                        $image->image_path         = $destinationPath . $filename;
                        $image->activitiesplaces_id = $this->activitiesplaces->id;
                        $image->save();
                    }
                }
                //store packages
                for ($i=0; $i < count(Input::get('package_name')); $i++) 
                { 
                    if (Input::has('package_name.'.$i)) 
                    {
                            $package=new Activitiesplacespackages;
                            $package->activitiesplaces_id = $this->activitiesplaces->id;
                            $package->name=Input::get('package_name.'.$i);
                            $package->price=Input::get('package_price.'.$i);
                            $package->description=Input::get('package_description.'.$i);
                            $package->save();
                    }      
                }
                
                // Redirect to the new blog post page
                return Redirect::to('admin/activitiesplaces/' . $this->activitiesplaces->id . '/edit')->with('success',"New Activity Added");
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/activitiesplaces/create')->with('error', "Error! Please Check.");
        }

        // Form validation failed
        return Redirect::to('admin/activitiesplaces/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getShow($activitiesplaces)
    {
        // Title
        $title = "Activity Place Details";
        $activitiesplacespackages=$this->activitiesplacespackages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
        $activitiesplacesimages=$this->activitiesplacesimages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
        // Show the page
        return View::make('admin/activitiesplaces/show', compact('activitiesplaces','activitiesplacespackages','activitiesplacesimages', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $post
     * @return Response
     */
    public function getEdit($activitiesplaces)
    {
        // Title
        $title = "Edit Activity";
        $activities=$this->activities->get();
        $activitiesplacesimages=$this->activitiesplacesimages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
        $activitiesplacespackages=$this->activitiesplacespackages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
        // Show the page
        return View::make('admin/activitiesplaces/edit', compact('activitiesplacesimages','activitiesplacespackages','activities','activitiesplaces', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $post
     * @return Response
     */
    public function postEdit($activitiesplaces)
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required|min:3',
            'description' => 'required|min:3'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the activitiesplaces data
            $activitiesplaces->name            = Input::get('name');
            $activitiesplaces->slug             = Str::slug(Input::get('name'));
            $activitiesplaces->description         = Input::get('description');
            $activitiesplaces->activities_id         = Input::get('activities_id');
            $activitiesplaces->youtube_link         = Input::get('price');
            
            // Was the activitiesplaces updated?
            if($activitiesplaces->save())
            {
                //get all images of the activityplace
                $activitiesplacesimages=$this->activitiesplacesimages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
                //Check if any previous image in the database 
                foreach ($activitiesplacesimages as $image) 
                {
                    //Suppose,image is not present in the submitted form
                    $exists="false";
                    for ($i=0; $i <count(Input::get('eimage_id')) ; $i++) 
                    { 
                        //Is the image present  in the form?
                        if($image->id==Input::get('eimage_id.'.$i))
                        {
                            //If,yes then true
                            $exists="true";
                        }
                    }

                    if($exists=="false")
                    {
                        //delete the image if $exists=false
                        $image->delete();
                    }
                }

                //get all packages of the activityplace
                $activitiesplacespackages=$this->activitiesplacespackages->where('activitiesplaces_id','=',$activitiesplaces->id)->get();
                //Check all previous packages in the database 
                foreach ($activitiesplacespackages as $package) 
                {
                    //Suppose,package is not present in the submitted form
                    $exists="false";
                    for ($i=0; $i <count(Input::get('epackage_id')) ; $i++) 
                    { 
                        //Is the package present  in the form?
                        if($package->id==Input::get('epackage_id.'.$i))
                        {
                            //If,yes then true
                            $exists="true";
                            //update the package data
                            $package->name            = Input::get('epackage_name.'.$i);
                            $package->price            = Input::get('epackage_price.'.$i);
                            $package->description            = Input::get('epackage_description.'.$i);
                            $package->save();
                            
                        }
                    }

                    if($exists=="false")
                    {
                        //delete the package if $exists=false
                        $package->delete();
                    }
                }

                //check for any new image added and upload,add to database
                for ($i=0; $i < count(Input::file('image')); $i++) 
                { 
                    $destinationPath = '';
                    $filename        = '';

                    if (Input::hasFile('image.'.$i)) 
                    {
                        $file            = Input::file('image.'.$i);
                        $destinationPath = 'img/activities/';
                        $filename        = str_random(6) . '_' . $file->getClientOriginalName();
                        
                        $uploadSuccess   = $file->move($destinationPath, $filename);
                        $image = new Activitiesplacesimages;
                        $image->image_path         = $destinationPath . $filename;
                        $image->activitiesplaces_id = $activitiesplaces->id;
                        $image->save();
                    }
                }

                //check for any package added and if any add to database
                for ($i=0; $i < count(Input::get('package_name')); $i++) 
                { 
                    if (Input::has('package_name.'.$i)) 
                    {
                            $package=new Activitiesplacespackages;
                            $package->name=Input::get('package_name.'.$i);
                            $package->price=Input::get('package_price.'.$i);
                            $package->description=Input::get('package_description.'.$i);
                            $package->activitiesplaces_id = $activitiesplaces->id;
                            $package->save();
                    }      
                }
                
                
                // Redirect to the new blog post page
              return Redirect::to('admin/activitiesplaces/' . $activitiesplaces->id . '/edit')->with('success', "Updated Successfull.");
            }

            // Redirect to the activitiesplaces post management page
            return Redirect::to('admin/activitiesplaces/' . $activitiesplaces->id . '/edit')->with('error', "Error! while Updating.");
        }

        // Form validation failed
        return Redirect::to('admin/activitiesplaces/' . $activitiesplaces->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function getDelete($activitiesplaces)
    {
        // Title
        $title = "Delete Activity Place Details";

        // Show the page
        return View::make('admin/activitiesplaces/delete', compact('activitiesplaces', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($activitiesplaces)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $activitiesplaces->id;
            $activitiesplaces->delete();

            // Was the activitie activitiesplaces deleted?
            $activitiesplaces = Activitiesplaces::find($id);
            if(empty($activitiesplaces))
            {
                // Redirect to the activitie posts management page
                return Redirect::to('admin/activitiesplaces')->with('success', "Delete Succesful");
            }
        }
        // There was a problem deleting the activitie post
        return Redirect::to('admin/activitiesplaces')->with('error', "Error! while deleting.");
    }
    
	public function getData()
    {
        $activitiesplaces = Activitiesplaces::select(array('activitiesplaces.id', 'activitiesplaces.name','activitiesplaces.activities_id as activity', 'activitiesplaces.youtube_link as video'));

        return Datatables::of($activitiesplaces)
        ->edit_column('activity', function ($activitiesplace) {
                $activityname= DB::table('activities')->select('name')->where('id', '=', $activitiesplace->activity)->first();
                return $activityname->name;
            })
        ->add_column('actions', 
            '<a href="{{{ URL::to(\'admin/activitiesplaces/\' . $id . \'/show\' ) }}}" class="btn btn-default btn-xs iframe" >Show</a>
            <a href="{{{ URL::to(\'admin/activitiesplaces/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >Edit</a>
            <a href="{{{ URL::to(\'admin/activitiesplaces/\' . $id . \'/delete\' ) }}}" class="btn btn-default btn-xs iframe" >Delete</a>
        ')
        ->edit_column('video', '<a target="_blank" href="https://www.youtube.com/embed/{{{$video}}}">View Video</a>')

        ->make();
    }

}