<?php

class PackagesController extends BaseController {

    /**
     * Activities Model
     * @var Activities
     */
    
    protected $packages;

    /**
     * User Model
     * @var User
     */
    protected $user;
	
    /**
     * Inject the models.
     * @param Activities $activities
     * @param User $user
     */
    public function __construct(User $user,Packages $packages)
    {
        parent::__construct();

        $this->packages = $packages;
        $this->user = $user;
    }
    
	/**
	 * Returns all the blog posts.
	 *
	 * @return View
	 */
	public function getView($packageslug)
	{
		$packages=$this->packages->where('slug','=',$packageslug)->first();
		$nonpackages=$this->packages->where('slug','<>',$packageslug)->get();
		return View::make('site/packages/view',compact('packages','nonpackages'));
	}

	}
